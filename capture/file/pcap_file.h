
#pragma once

#include <stdint.h>
#include <stdbool.h>
#include <pcap/pcap.h>

typedef void (*packet_do)(uint8_t * buf, int len);

void file_do_function(u_char *user, const struct pcap_pkthdr *h, const u_char *bytes);

bool file_loop(char * pcapfile, packet_do f);


