
#include "pcap_file.h"


void file_do_function(u_char *user, const struct pcap_pkthdr *h, const u_char *bytes)
{
	packet_do  p = (packet_do)user;

	static int packet_index = 0;

	packet_index ++;

	//printf("==== %d\n",packet_index);
	p( (uint8_t*)bytes, h->caplen);
}
                                  

bool file_loop(char * pcapfile, packet_do f)
{
	pcap_t * p;
	char errbuf[1600];
	
	p = pcap_open_offline(pcapfile, errbuf);
	if(p == NULL)
		return false;
	else
	{
		int ret = 0;

		while(pcap_dispatch(p, 1, file_do_function, (u_char *)f) == 1)
		{
		}
		return true;		
	}
}


