
TOPDIR = .

SUBDIRS1 =  common capture decodelayer http
SUBDIRS2 =  build

SUBDIRS = $(SUBDIRS1) $(SUBDIRS2)

-include $(TOPDIR)/Makefile.rule

$(SUBDIRS2): $(SUBDIRS1)

