
#include <stdint.h>
#include <string.h>
#include <arpa/inet.h>

#include "mid.h"
#include "run_alloc.h"
#include "decode_layer_struct.h"
#include "decode_protocol.h"

enum decode_protocol_t decode_ip( struct packet_info ** pinfo)
{
	uint8_t * buf = (*pinfo)->curinfo.buf;
	int len = (*pinfo)->curinfo.len;
	
	uint8_t * pend = buf+len;
	uint8_t * p = buf;

	uint16_t ipheadlen = ((buf[0]&0x0F)<<2);
	uint16_t ippacketlen = ntohs(*(uint16_t*)(buf+2));

	(*pinfo)->ipinfo.src = ntohl( *(uint32_t*)(&buf[12]) );
	(*pinfo)->ipinfo.dst = ntohl( *(uint32_t*)(&buf[16]) );

	uint8_t protocol = (buf[9]);

	(*pinfo)->curinfo.len = ippacketlen - ipheadlen;
	memmove(buf, buf+ipheadlen, (*pinfo)->curinfo.len);

	switch(protocol)
	{
	case 6:
		return DECODE_PROTOCOL_TCP;
	case 17:
		return DECODE_PROTOCOL_UDP;
	default:
		return DECODE_PROTOCOL_NONE;
	}
}


