
#include <stdint.h>
#include <string.h>

#include "mid.h"
#include "run_alloc.h"
#include "decode_layer_struct.h"

#include "decode_protocol.h"

enum decode_protocol_t decode_mac( struct packet_info ** pinfo)
{
	uint8_t * buf = (*pinfo)->curinfo.buf;
	int len = (*pinfo)->curinfo.len;
	
	uint8_t * pend = buf;
	uint8_t * p = buf+12;

	uint16_t type = *(uint16_t*)p;

decode_vlan:
	if(type == 0x8100)
	{
		p += 4;	
		if(p >= buf)
			return DECODE_PROTOCOL_NONE;

		type = *(uint16_t*)p;
		if(type == 16)
		{
			type = 0x8100;
			goto decode_vlan;
		}
		else
		{
			return DECODE_PROTOCOL_NONE;
		}
	}
	else
	{
		p += 2;
		
		(*pinfo)->curinfo.len = len-(p-buf);
		memmove(buf,p,(*pinfo)->curinfo.len);
		return DECODE_PROTOCOL_IP;
	}
	
}


