

#include <stdint.h>
#include "mid.h"
#include "run_base.h"

#include "decode_protocol.h"
#include "decode_layer_struct.h"

enum decode_protocol_t decode_mac( struct packet_info ** pinfo);
enum decode_protocol_t decode_ip( struct packet_info ** pinfo);
enum decode_protocol_t decode_tcp( struct packet_info ** pinfo);


void decode_all(uint8_t * buf, int len)
{
	struct packet_info * pinfo = (struct packet_info *)run_malloc(MID_DECODE, sizeof(struct packet_info));
	memset(pinfo,0,sizeof(struct packet_info));
	pinfo->curinfo.buf = (uint8_t*)run_malloc(MID_DECODE,len+10);
	memcpy(pinfo->curinfo.buf, buf, len);
	pinfo->curinfo.len = len;
	
	enum decode_protocol_t proto = decode_mac(&pinfo);

	while(proto != DECODE_PROTOCOL_NONE)
	{
		switch(proto)
		{
		case DECODE_PROTOCOL_IP:
			proto = decode_ip(&pinfo);
			break;
		case DECODE_PROTOCOL_TCP:
			proto = decode_tcp(&pinfo);
			break;
		case DECODE_PROTOCOL_HTTP:
			//proto = decode_http(&pinfo);
			break;
		default:
			proto = DECODE_PROTOCOL_NONE;
			break;
		}
	}
	if(pinfo != NULL)
	{
		run_free(pinfo->curinfo.buf);
		run_free(pinfo);
	}
}

