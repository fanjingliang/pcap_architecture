
#pragma once

#include <stdint.h>

#include "decode_protocol.h"

struct mac_layer_info
{
};

struct ip_layer_info
{
	uint32_t	src;
	uint32_t	dst;
};

struct tcp_layer_info
{
	uint16_t sport;
	uint16_t dport;
	
	uint32_t seq;
	uint32_t ack;
	uint8_t flags;
};

struct current_packet_info
{
	uint8_t * buf;
	int len;
};

struct packet_info
{
	struct mac_layer_info macinfo;
	struct ip_layer_info  ipinfo;
	struct tcp_layer_info tcpinfo;
	
	struct current_packet_info curinfo;
};


struct IPv4ConnectAddr
{
	uint32_t src;
	uint32_t dst;
	uint16_t sport;
	uint16_t dport;
};


