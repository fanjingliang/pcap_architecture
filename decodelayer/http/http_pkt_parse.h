/********************************************************
*filename: http_head_decode.h
*create: zxiaodong
*time: 2016-12-08
*info: http头部解析相关数据接口以及类
********************************************************/

#ifndef __HTTP_HEAD_DECODE_H__
#define __HTTP_HEAD_DECODE_H__

#include "mid.h"

#include <stdint.h>
#include "run_alloc.h"
#include "run_base.h"


#define COMMON_FIELD_FUNC_HASH_CNT				50
#define HTTP_ALL_HEAD_FIELD_HASH_BUCKET_CNT		30


//HTTP报文类型：
typedef enum
{
	HTTP_PACKET_TYPE_HEAD_FIELD = 0,
	HTTP_PACKET_TYPE_HEADER = 1,
	HTTP_PACKET_TYPE_BODY,
	HTTP_PACKET_TYPE_URI,
	HTTP_PACKET_TYPE_URL,
	HTTP_PACKET_TYPE_OTHER
}HTTP_PACKET_TYPE_E;


//HTTP头部类型：
typedef enum
{
	HTTP_HEADER_OTHER = 0,
	HTTP_HEADER_AUTHORIZATION = 1,
	HTTP_HEADER_CONNECTION,
	HTTP_HEADER_CONTENT_ENCODING,
	HTTP_HEADER_CONTENT_LENGTH,
	HTTP_HEADER_CONTENT_TYPE,
	HTTP_HEADER_COOKIE,
	HTTP_HEADER_HOST,
	HTTP_HEADER_LOCATION,
	HTTP_HEADER_PROXY_CONNECTION,
	HTTP_HEADER_REFERER, 
	HTTP_HEADER_TRANSFER_ENCODING,
	HTTP_HEADER_USER_AGENT,
	HTTP_HEADER_X_REQUESTED_FROM,
	HTTP_HEADER_SET_COOKIE,
	HTTP_HEADER_MAX
}HTTP_HEADER_E;


//每一个头部字段对应的节点结构体：
typedef struct 
{
	run_uint_t        hash;
	run_str_t         key;
	run_str_t         value;
	run_uint_t        *lowcase_key;
}http_head_elt_t;


//header结构体：
typedef struct
{
	run_str_t         head;
	run_hash_t       *all_headers;   //保存所有解析出的头部字段
	http_head_elt_t* common_headers[HTTP_HEADER_MAX];  //保存命中的通用头部字段
}http_headers_t;


//定位类型: 头部字段解析函数
typedef run_int_t (* http_header_field_func )(http_headers_t *r, http_head_elt_t *h, run_uint_t type);


//头部字段解析函数节点
typedef struct 
{
    run_str_t						name;
    run_uint_t						type;
    http_header_field_func			handler;
}http_header_init_t;


/*body 结构体*/
typedef struct
{
	run_str_t body;
	//待扩展,后续可能解析结构化数据
	
}http_parse_body_t;


#define HASH_KEY_LEN_MAX	64

inline unsigned int http_head_field_hash_key(const void *pkey)
{
	run_str_t *key = (run_str_t *)pkey;
	
	assert(key!=NULL && key->data!=NULL);

	char strkey[HASH_KEY_LEN_MAX] = {0};
	if(key->len < HASH_KEY_LEN_MAX)
	{
		strncpy(strkey, (const char*)key->data, key->len);
	}
	else
	{
		strncpy(strkey, (const char*)key->data, HASH_KEY_LEN_MAX-1);
	}
	
	return djb_hash((const unsigned char *)strkey);	
}


inline int http_head_field_hash_compare(const void *pkey1, const void *pkey2)
{
	run_str_t *key1 = (run_str_t *)pkey1;
	run_str_t *key2 = (run_str_t *)pkey2;
	
	if(key1 == NULL || key2 == NULL || key1->len != key2->len)
		return 0;

	//if(0 == strncasecmp((char *)key1->data, (char *)key2->data, key1->len))
	if(0 == run_strncasecmp((unsigned char *)key1->data, (unsigned char *)key2->data, key1->len))
		return 1;

	return 0;
}

inline void http_head_field_hash_val_free(void *obj){
	run_free(obj);
}


class CHttpPacket
{
public:
	CHttpPacket(){
		init();
	}
	virtual ~CHttpPacket(){
		close();
	}
	virtual void init(){
		const int bucket_cnt = HTTP_ALL_HEAD_FIELD_HASH_BUCKET_CNT;

		m_headers.all_headers = run_hash_init(bucket_cnt,
									http_head_field_hash_key,
									http_head_field_hash_compare,
									NULL,NULL,NULL,http_head_field_hash_val_free);
		assert(m_headers.all_headers != NULL);
		memset(m_headers.common_headers, 0, sizeof(http_head_elt_t*)*HTTP_HEADER_MAX);
		memset(&m_body, 0, sizeof(http_parse_body_t));
	}
	virtual void close(){
		assert(m_headers.all_headers != NULL);
		if (m_headers.all_headers != NULL){
			run_hash_free(m_headers.all_headers);
			m_headers.all_headers = NULL;
		}
	}
	http_head_elt_t* get_head_field_for_name(char *name);
	run_str_t get_head_field_for_enum(HTTP_HEADER_E type);

	virtual int http_head_parse(unsigned char *pbuf, uint32_t len)=0;
	virtual int http_body_parse(unsigned char *pbuf, uint32_t len){
		m_body.body.data = pbuf;
		m_body.body.len = len;
	}

	run_str_t  get_body(){return (m_body.body);}
	run_str_t  get_head(){return (m_headers.head);}
	
protected:
	http_headers_t m_headers;
	http_parse_body_t m_body;
	
public:
	//类函数接口
	static int http_parse_header_line(char *pbuf, int len, http_headers_t &headers);
	static int http_parse_request_line(char *buf, int nLen, int &method, run_str_t &URI, int &httpVersion);
	static int http_parse_status_line(char *buf, int len, int &statusCode);
	static http_header_init_t* find_common_header(run_str_t *header_name);


	//初始化时调用
	static void http_pkt_parse_init();
	static void http_pkt_parse_close();
	HTTP_PACKET_TYPE_E get_packet_type_eunm_for_string(char* packet_type);

private:
	static run_hash_t* m_common_header_int_hash; 
};

class CHttpRequestPacket: public CHttpPacket
{
public:
	
	CHttpRequestPacket(){
		init();
	}
	~CHttpRequestPacket(){
		close();
	}

	void reset(){
		CHttpPacket::close();
		close();

		CHttpPacket::init();
		init();
	}
	
	virtual int http_head_parse(unsigned char *pbuf, uint32_t len);

	run_str_t  get_uri(){return m_URI;}
	int get_method(){return m_method;}
	
private:
	void init(){
		m_method = 0;
		memset(&m_URI, 0, sizeof(run_str_t));
		m_httpVersion = 0;
	}

	void close(){
	}

private:
	int m_method;
	run_str_t m_URI;
	int m_httpVersion;
};


class CHttpResponsePacket: public CHttpPacket
{
public:
	CHttpResponsePacket(){
		init();
	}
	~CHttpResponsePacket(){
		close();
	}
	
	void reset(){
		CHttpPacket::close();
		close();

		CHttpPacket::init();
		init();
	}
	virtual int http_head_parse(unsigned char *pbuf, uint32_t len);
	int get_status(){return m_statusCode;}

private:
	void init(){
		m_statusCode = 0;
	}

	void close(){
	}
	
private:
	int m_statusCode;
	
};

#endif	/* !__HTTP_HEAD_DECODE_H__ */

