
#include <stdio.h>
#include <stdint.h>
#include <arpa/inet.h>

#include <map>
#include "run_base.h"
#include "decode_layer_struct.h"
#include "decode_protocol.h"
#include "http_decode.h"
#include "httptemplate.h"


typedef std::map<std::string, CHTTPDecode *> MAP_HTTPDECODES;

MAP_HTTPDECODES maphttpdecode;

CHTTPDecode *g_httpdecode_msg;

//在pos时调用
run_str_t http_get_pos_info(bool isack, char *head_name)
{
	run_str_t tmp_str;
	
	if( strcasecmp(head_name, "BODY") == 0)
	{
		if(!isack)
		{	
			tmp_str = g_httpdecode_msg->get_req_body();
		}
		else
		{	
			tmp_str = g_httpdecode_msg->get_rsp_body();
		}
	}
	else if( strcasecmp(head_name, "URI") == 0)
	{
		tmp_str = g_httpdecode_msg->get_uri();
	}
	else if( strcasecmp(head_name, "URL") == 0)
	{
		tmp_str = g_httpdecode_msg->get_url();
	}
	else
	{
		if(!isack)
		{	
			tmp_str = g_httpdecode_msg->get_req_header(head_name);
		}
		else
		{	
			tmp_str = g_httpdecode_msg->get_rsp_header(head_name);
		}
	}	

	return tmp_str;
}


void http_init()
{
	httptemplate::init();
	httptemplate::read_script_dir("../tpt/");
	httptemplate::init_finish();

	set_template_interface(NULL,http_get_pos_info);
	CHttpPacket::http_pkt_parse_init();
}

void EspDataProcess(CHTTPDecode * m_pHttpDecode)
{
	/*检查解码对象是否有效*/
	run_str_t host = m_pHttpDecode->get_host();
	run_str_t uri = m_pHttpDecode->get_uri();
	
	if ((host.len == 0) ||(uri.len == 0) )
	{	
		//如果解码有错，不处理
		return ;
	}

	g_httpdecode_msg = m_pHttpDecode;

	/*模板匹配*/
	httptemplate *pTptArray[HTTP_MAX_RET_TPT];
	int nTpt;

	if(host.data != NULL)
	{
		char tmp = host.data[host.len];
		host.data[host.len] = '\0';
		nTpt = httptemplate::findtemplate(uri, m_pHttpDecode->is_post() ? POST:GET, host, (void **)pTptArray);
		host.data[host.len] = tmp;
	}
	else
	{
		nTpt = httptemplate::findtemplate(uri, m_pHttpDecode->is_post() ? POST:GET, host, (void **)pTptArray);
	}

	
	if(nTpt >0) 
	{
		printf("template hit[%d]!\n", nTpt);
		/*模板提取*/
		for (int i=0;i<nTpt;i++ )
		{
			printf("template name[%s]!\n", pTptArray[i]->get_template_name());
			pTptArray[i]->parse();
		}
	}
}

enum decode_protocol_t decode_http(struct packet_info ** pinfo)
{
	char buf[128] = {0};

	sprintf(buf,"%d.%d.%d.%d",(*pinfo)->ipinfo.src,(*pinfo)->ipinfo.dst,(*pinfo)->tcpinfo.sport,(*pinfo)->tcpinfo.dport);
	std::string strcon = buf;
	
	CHTTPDecode *  phttpdecode;
	
	MAP_HTTPDECODES::iterator itr = maphttpdecode.find(strcon);
	if (itr != maphttpdecode.end())
	{
		phttpdecode = itr->second;
	}
	else
	{
		phttpdecode = new CHTTPDecode(1);
		maphttpdecode[strcon] = phttpdecode;
		
		sprintf(buf,"%d.%d.%d.%d",(*pinfo)->ipinfo.dst,(*pinfo)->ipinfo.src,(*pinfo)->tcpinfo.dport,(*pinfo)->tcpinfo.sport);
		strcon = buf;

		maphttpdecode[strcon] = phttpdecode;
	}

	bool clienttoserver = false;
	if( (*pinfo)->tcpinfo.dport == 80)
	{
		clienttoserver = true;
	}
	
	if (!phttpdecode->is_mine((*pinfo)->curinfo.buf, (*pinfo)->curinfo.len ,clienttoserver)){
		phttpdecode->force_reassemble();
		EspDataProcess(phttpdecode);
		phttpdecode->reset();
	}
	phttpdecode->add_pkt((*pinfo)->curinfo.buf, (*pinfo)->curinfo.len ,clienttoserver, ntohl((*pinfo)->tcpinfo.seq));
	if (phttpdecode->is_done()){
		EspDataProcess(phttpdecode);
		phttpdecode->reset();
	}
		
	return DECODE_PROTOCOL_NONE;
}

