/********************************************************************
* 文件名称：http_def.h
* 创建人员：tianhuimeng
* 创建日期：2016-12-08
* 文件描述：HTTP全局宏、结构体定义
*********************************************************************/
#ifndef __HTTP_DEF_H__
#define __HTTP_DEF_H__

#include <stdint.h>

//常量
const char g_const_str_get[]="GET ";
const char g_const_str_post[]="POST ";
const char g_const_str_put[] = "PUT ";
const char g_const_str_http[]="HTTP/1.";

#define RUN_MAX_OFF_UINT32_VALUE 4294967296 
#define HTTP_HEAD_END_FLAG_LEN 4

#define MID_HTTP_REASSEMBLE 1

#define DIRECTION_CLIENT_TO_SERVER 1
#define DIRECTION_SERVER_TO_CLIENT 0


#ifdef _DEBUG_HTTP
	#define  HTTP_DEBUG(code) code
#else
	#define HTTP_DEBUG(code)
#endif

extern uint32_t g_max_body_buf_len;

/*可配置*/
inline void set_max_body_buf_len(uint32_t max_body_len){
	g_max_body_buf_len = max_body_len;
}

typedef enum
{
	HTTP_PKT_TYPE_UNKNOW = 0,
	HTTP_PKT_TYPE_REQ_POST,
	HTTP_PKT_TYPE_REQ_GET,
	HTTP_PKT_TYPE_REQ_PUT,
	HTTP_PKT_TYPE_RESPONSE,
	HTTP_PKT_TYPE_MAX,
}HTTP_PKT_TYPE_E;


inline int http_get_pkt_type(unsigned char *buf, uint32_t buf_len)
{
	int pkt_type = HTTP_PKT_TYPE_UNKNOW;
	
	if (buf_len <=4){
		return pkt_type;
	}
	
	if ((*(int32_t*)buf == *(int32_t*)g_const_str_http)
		&& (buf_len >= 9 && buf[8]==' ' && *(int32_t*)(buf+3) ==  *(int32_t*)(g_const_str_http+3)))
	{
		pkt_type = HTTP_PKT_TYPE_RESPONSE;
	}
	else if (*(int32_t*)buf == *(int32_t*)g_const_str_get){
		pkt_type = HTTP_PKT_TYPE_REQ_GET;
	}
	else if ((*(int32_t*)buf == *(int32_t*)g_const_str_post)
		&& (buf_len>=5 && buf[4]==' '))
	{
		pkt_type = HTTP_PKT_TYPE_REQ_POST;
	}
	else if (*(int32_t*)buf == *(int32_t*)g_const_str_put){
		pkt_type = HTTP_PKT_TYPE_REQ_PUT;	
	}
	
	return pkt_type;
}


#endif // __HTTP_DEF_H__
