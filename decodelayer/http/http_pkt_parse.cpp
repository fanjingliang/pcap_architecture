/********************************************************
*filename: http_head_decode.cpp
*create: zxiaodong
*time: 2016-12-08
*info: http头部解析相关接口
********************************************************/

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdint.h>

#include "mid.h"
#include "run_alloc.h"
#include "http_pkt_parse.h"
#include "http_def.h"

#define HTTP_HEAD_DECODE_LEN_MAX 		4096

//#define HTTP_HEAD_DECODE_DUG	" "


static const char s_c0x0d_0x0a[2]={0x0d,0x0a};

run_hash_t* CHttpPacket::m_common_header_int_hash = NULL;

static run_int_t
run_http_process_header_line(http_headers_t *r, http_head_elt_t *h, run_uint_t type);


http_header_init_t  run_http_request_headers[] = {
		{ run_string("authorization"), HTTP_HEADER_AUTHORIZATION, run_http_process_header_line},		 
		{ run_string("connection"), HTTP_HEADER_CONNECTION, run_http_process_header_line },
		{ run_string("content-encoding"), HTTP_HEADER_CONTENT_ENCODING, run_http_process_header_line },	
		{ run_string("content-length"), HTTP_HEADER_CONTENT_LENGTH , run_http_process_header_line},	
		{ run_string("content-type"), HTTP_HEADER_CONTENT_TYPE , run_http_process_header_line},		
		{ run_string("cookie"), HTTP_HEADER_COOKIE , run_http_process_header_line},
		{ run_string("host"),  HTTP_HEADER_HOST, run_http_process_header_line },
		{ run_string("location"), HTTP_HEADER_LOCATION, run_http_process_header_line },
		{ run_string("proxy-Connection"), HTTP_HEADER_PROXY_CONNECTION, run_http_process_header_line },
		{ run_string("referer"), HTTP_HEADER_REFERER, run_http_process_header_line },		 
		{ run_string("transfer-encoding"),  HTTP_HEADER_TRANSFER_ENCODING, run_http_process_header_line },
		{ run_string("user-agent"), HTTP_HEADER_USER_AGENT, run_http_process_header_line},
		{ run_string("x-requested-from"),  HTTP_HEADER_X_REQUESTED_FROM, run_http_process_header_line },
	};


http_header_init_t  run_http_response_headers[] = {
		{ run_string("connection"), HTTP_HEADER_CONNECTION, run_http_process_header_line},
		{ run_string("content-disposition"), HTTP_HEADER_CONTENT_ENCODING, run_http_process_header_line},	
		{ run_string("content-encoding"), HTTP_HEADER_CONTENT_ENCODING, run_http_process_header_line},	
		{ run_string("content-length"), HTTP_HEADER_CONTENT_LENGTH, run_http_process_header_line},	
		{ run_string("content-type"), HTTP_HEADER_CONTENT_TYPE, run_http_process_header_line},		
		{ run_string("set-cookie"), HTTP_HEADER_SET_COOKIE, run_http_process_header_line},
		{ run_string("location"), HTTP_HEADER_LOCATION, run_http_process_header_line},
		{ run_string("transfer-encoding"),  HTTP_HEADER_TRANSFER_ENCODING, run_http_process_header_line}
	};


/*============================================
*函数名:run_http_process_header_line
*参数: http_headers_t *r [IN]
*返回值: 状态 0, 1
*功能:把解析后的字段保存到公共字段集中
*说明:头部字段赋值函数
*创建日期: 2016-12-08
*修改人员: zxiaodong
============================================*/
static run_int_t
run_http_process_header_line(http_headers_t *r, http_head_elt_t *h, run_uint_t type)
{
    http_head_elt_t  **ph;

    ph = (http_head_elt_t **) (&r->common_headers[type]);

	//mod by tianhuimeng for zentao bug 2109 (segment fault)
    //if (*ph == NULL) 
	//{
        *ph = h;
    //}

    return 0;
}

void CHttpPacket::http_pkt_parse_init()
{
	int common_header_num = sizeof(run_http_request_headers)/sizeof(http_header_init_t);

	//初始化
	m_common_header_int_hash = run_hash_init(COMMON_FIELD_FUNC_HASH_CNT,
							http_head_field_hash_key,
							http_head_field_hash_compare,
							NULL,NULL,NULL,NULL);
	if(NULL == m_common_header_int_hash)
	{
		return;
	}

	//加载
	for(int i=0; i<common_header_num; i++)
	{
		run_hash_add(m_common_header_int_hash, (void *)(&run_http_request_headers[i].name), (void *)(&run_http_request_headers[i]));
	}
	return;
}

void CHttpPacket::http_pkt_parse_close()
{
	if (m_common_header_int_hash != NULL){
		run_hash_free(m_common_header_int_hash);
		m_common_header_int_hash = NULL;
	}
}

http_header_init_t* CHttpPacket::find_common_header(run_str_t *header_name)
{	
	return (http_header_init_t *)run_hash_find(m_common_header_int_hash, (void *)header_name);
}


/*============================================
*函数名:http_parse_request_line
*参数: char *buf, int nLen, int &method, run_str_t &URI, int &httpVersion
*返回值: 成功:0, 失败-1
*功能:http请求报文中请求行的解析,保存
*说明:  POST /q?h=D9A670CD6A15DCCB842C30B27C855BBB&r=0000&v=6.3.8.22562 HTTP/1.1
*创建日期: 2016-12-08
*修改人员: zxiaodong
============================================*/
int CHttpPacket::http_parse_request_line(char *buf, int nLen, int &method,
									run_str_t &URI, int &httpVersion)
{
	static char szGet[5]="GET ";
	static char szPost[5]="POST";
	static char szPut[5] = "PUT ";
	static char szHttp[8]="HTTP/1.";

	
	method = HTTP_PKT_TYPE_UNKNOW;
	if (nLen<4) 
		return -1;

	//获取method
	if( *(uint32_t*)buf == *(uint32_t*)szGet) 
	{
		method = HTTP_PKT_TYPE_REQ_GET;
	}
	else if( *(uint32_t*)buf == *(uint32_t*)szPost )
	{
		if(nLen>=5 && buf[4]==' ') 
			method = HTTP_PKT_TYPE_REQ_POST;
	}
	else if(*(uint32_t*)buf == *(uint32_t*)szPut)
	{
		method = HTTP_PKT_TYPE_REQ_PUT;
	}

	char *p1 = buf;
	char *p2;
	int tmplen;

	if(NULL == (p2 = strchr(p1, ' ')))
		return 0;
	
	tmplen = p2 - p1;
	nLen -= tmplen;
	p1 = p2+1;  //偏移位置

	//URI
	if(NULL == (p2 = strchr(p1, ' ')))
		return 0;

	URI.data = (unsigned char *)p1;
	URI.len = tmplen = p2 - p1;

	nLen -= tmplen;
	p1 = p2+1;  //偏移位置

	//HTTPVersion
	if( *(uint32_t*)p1 == *(uint32_t*)szHttp )
	{
		//if( nLen >= 8 && *(uint32_t*)(p1+3) ==  *(uint32_t*)(szHttp+3) ) 
			//httpVersion = HEAD_HTTP;
	}

	return 0;
}

/*============================================
*函数名:http_parse_status_line
*参数: char *buf, int len, int &statusCode
*返回值: 成功:0, 失败-1
*功能: http响应报文中状态行的解析,保存
*说明:  HTTP/1.1 200 OK
*创建日期: 2016-12-08
*修改人员: zxiaodong
============================================*/
int CHttpPacket::http_parse_status_line(char *buf, int len, int &statusCode)
{
	char *p1;

	if(NULL == (p1 = strchr(buf, ' ')))
	{
		goto err;
	}

	statusCode = atoi(p1);  //自动跳过空格,遇到非数字字符停止
	
	return 0;

err:
	//异常处理
	return -1;
}


/*============================================
*函数名:http_parse_header_line
*参数: char *pbuf, int len, http_headers_t &headers, run_hash_t *func_assemble
*返回值: 成功:0, 失败-1
*功能: http请求/响应报文中头部字段的解析,保存
*说明:  Content-Type: application/x-www-form-urlencoded
*创建日期: 2016-12-08
*修改人员: zxiaodong
============================================*/
int CHttpPacket::http_parse_header_line(char *pbuf, int len, http_headers_t &headers)
{
	char *p1 = pbuf;
	char *p2;
	http_head_elt_t *node;


	if(NULL == pbuf)
	{
		//打印异常日志
		goto err;
	}
	
	if(NULL == (p2 = strchr(p1, ':')))
	{
		//打印异常日志
		goto err;
	}
	
	node = (http_head_elt_t *)run_malloc(MID_CHTTP_HEAD_DECODE, sizeof(http_head_elt_t));
	if(NULL == node)
	{	
		//打印异常日志
		goto err;
	}

	node->key.data = (unsigned char *)p1;
	node->key.len  = p2 - p1;

	//如果是大写字母则转换成小写字母
	for(unsigned int i=0; i<node->key.len; i++)
	{
		*(node->key.data + i) = run_tolower( *(node->key.data + i) );
	}
	
	node->value.data = (unsigned char *)(p2+2);
	node->value.len  = len - (p2 - p1) -2;

	node->hash = http_head_field_hash_key((void *)&node->key);
	node->lowcase_key = NULL;

	//添加头部字段
	/*---xiaodong 后续需要考虑重复的头部字段问题(名字相同), 比如: 
	Cache-control: no-cache
	Cache-Control: no-cache
	or
	Content-Type: application/octet-stream
	Content-Type: text/plain;charset=utf-8
	当前处理为后一个覆盖前一个*/
	run_hash_update(headers.all_headers, (void *)(&node->key), (void *)node);
	

	//匹配并添加公共头部字段
	http_header_init_t *p_func;	
	p_func = find_common_header(&node->key);
	if(NULL != p_func && NULL != p_func->handler)
	{	
		p_func->handler(&headers, node, p_func->type);
	}
	
	return 0;
	
err:
	//异常处理
	return -1;
}


//提供接口:通过name查找hash获取头部字段
http_head_elt_t* CHttpPacket::get_head_field_for_name(char *name)
{
	run_str_t key;
	
	key.data = (unsigned char *)name;
	key.len = strlen(name);

	//查找Hsah获取
	return (http_head_elt_t *)run_hash_find(m_headers.all_headers, (void *)&key);
}

//提供接口:通过枚举变量从公共字段数组获取头部字段
run_str_t CHttpPacket::get_head_field_for_enum(HTTP_HEADER_E type)
{
	//直接获取
	if (m_headers.common_headers[type] != NULL){
		return (m_headers.common_headers[type]->value);
	}

	run_str_t tmp;
	run_str_null(&tmp);
	return tmp;
}


//提供接口:通过报文类型的字符串获取报文类型的枚举值
HTTP_PACKET_TYPE_E CHttpPacket::get_packet_type_eunm_for_string(char* packet_type)
{
	//通过比较字符串把类型字符串转换成枚举值
	if(NULL == packet_type)
		return HTTP_PACKET_TYPE_HEAD_FIELD;

	if(packet_type[0] == 'h' || packet_type[0] == 'H')
		return HTTP_PACKET_TYPE_HEADER;  //head or Head
	else if(packet_type[0] == 'b' || packet_type[0] == 'B')
		return HTTP_PACKET_TYPE_BODY;   //body or Body
	else if(packet_type[0] == 'u' || packet_type[0] == 'U')
		return HTTP_PACKET_TYPE_URI;    //url or Url
	else
		return HTTP_PACKET_TYPE_OTHER;
}

/*//提供接口:通过报文中头部字段类型的字符串获取头部字段类型的枚举值
HTTP_HEADER_E CHttpPacket::get_packet_head_type_eunm_for_string(char* head_type)
{
	//通过比较字符串把类型字符串转换成枚举值
	if(NULL == head_type)
		return HTTP_HEADER_OTHER;

	if(head_type[0] == 'h' || head_type[0] == 'H')
		return HTTP_HEADER_HOST;  
	else if(head_type[0] == 'b' || head_type[0] == 'B')
		return HTTP_HEADER_COOKIE;   
	else if(head_type[0] == 'u' || head_type[0] == 'U')
		return HTTP_HEADER_CONTENT_TYPE;    
	else
		return HTTP_HEADER_OTHER;
}*/


/*============================================
*函数名:get_first_line_and_second_pointer
*参数: char *pStart,int nLen, int &nLineLength
*返回值: 下一行的起始地址
*功能: 获取http报文头部中当前行的范围
*说明:  
*创建日期: 2016-12-08
*修改人员: zxiaodong
============================================*/
char* get_first_line_and_second_pointer(char *pStart,int nLen, int &nLineLength)
{
	char *p_second = NULL;

	if(NULL == pStart || nLen <= 0)
		return p_second;

	nLineLength = 0;
	if(NULL != (p_second = (char *)memchr(pStart, 0x0a, nLen)))
	{
		nLineLength = p_second - pStart + 1;
	}
	
	return p_second;
}


/*============================================
*函数名:HttpRequestPacket
*参数: char *buf, int nLen
*返回值: 成功0,失败-1
*功能: 解析http请求报文的头部
*说明: 调用基本解析接口
*创建日期: 2016-12-08
*修改人员: zxiaodong
============================================*/
int CHttpRequestPacket::http_head_parse(unsigned char *buf, uint32_t nLen)
{
	int  nLeft = nLen;
	int  nLineLength;

	char *p1 = (char *)buf;
	char *p2;

	m_headers.head.data = buf;
	m_headers.head.len = nLen;
	
	p2 = get_first_line_and_second_pointer(p1, nLeft, nLineLength);
	if( nLineLength <= 0 || nLineLength >HTTP_HEAD_DECODE_LEN_MAX || NULL == p2)
	{
		//异常处理
		goto err_1;
	}

	//解析请求行
	http_parse_request_line(p1, nLineLength-2, m_method, m_URI, m_httpVersion); //-2是为了去掉回车换行

	nLeft -= nLineLength;  //偏移到下一行
	p1 = p2+1;
	
	while(nLeft>0)
	{
		p2 = get_first_line_and_second_pointer(p1, nLeft, nLineLength);			
		if( nLineLength <= 0 || nLineLength >4096 || NULL == p2)
		{
			//打印异常日志
			goto err_1;
		}

		//判断头部是否结束
		if( nLineLength==2 && *(short*)p1 == *(short*)s_c0x0d_0x0a )
		{
			nLeft = nLeft-2;  //设置标识: 头部解析完成
			break;
		}

		//解析头部字段行
		http_parse_header_line(p1, nLineLength-2, m_headers);  //-2是为了去掉回车换行

		nLeft -= nLineLength;   //偏移到下一行
		p1 = p2+1;	
	}
	
	return 0;
	
err_1:
	//异常处理	
	return -1;
}


/*============================================
*函数名:HttpResponsePacket
*参数: char *buf, int nLen
*返回值: 成功0,失败-1
*功能: 解析http响应报文的头部
*说明: 调用基本解析接口
*创建日期: 2016-12-08
*修改人员: zxiaodong
============================================*/
int CHttpResponsePacket::http_head_parse(unsigned char *pbuf, uint32_t len)
{
	int  nLeft = len;
	int  nLineLength;

	char *p1 = (char *)pbuf;
	char *p2;

	m_headers.head.data = pbuf;
	m_headers.head.len = len;

	
	p2 = get_first_line_and_second_pointer(p1, nLeft, nLineLength);
	if( nLineLength <= 0 || nLineLength >4096 || NULL == p2)
	{
		//异常处理
		goto err_1;
	}

	//解析请求行
	http_parse_status_line(p1, nLineLength-2, m_statusCode); //-2是为了去掉回车换行

	nLeft -= nLineLength;  //偏移到下一行
	p1 = p2+1;
	
	while(nLeft>0)
	{
		p2 = get_first_line_and_second_pointer(p1, nLeft, nLineLength);			
		if( nLineLength <= 0 || nLineLength >4096 || NULL == p2)
		{
			//打印异常日志
			goto err_1;
		}

		//判断头部是否结束
		if( nLineLength==2 && *(short*)p1 == *(short*)s_c0x0d_0x0a )
		{
			nLeft = nLeft-2;  //设置标识: 头部解析完成
			break;
		}

		//解析头部字段行
		if( p1[nLineLength-2] == 0x0d )
			http_parse_header_line(p1, nLineLength-2, m_headers);  //去掉回车换行
		else 
			http_parse_header_line(p1, nLineLength-1, m_headers);
		
		nLeft -= nLineLength;   //偏移到下一行
		p1 = p2+1;	
	}
	
	return 0;
	
err_1:
	//异常处理	
	return -1;
}



