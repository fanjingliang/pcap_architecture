/********************************************************************
* 文件名称：http_reassemble.cpp
* 创建人员：tianhuimeng
* 创建日期：2016-12-08
* 文件描述：http报文重组
*********************************************************************/
#include <stdint.h>

#include "http_def.h"
#include "http_reassemble.h"
#include "run_alloc.h"


uint32_t g_max_body_buf_len = (8*1024*1024);

void CHTTPReassemble::init(uint32_t is_response)
{
	run_str_null(&m_head);
	run_str_null(&m_body);
	
	m_content_len = 0;
	m_exp_seq = 0;
	set_status(STATUS_INIT);
	m_pkt_type = HTTP_PKT_TYPE_UNKNOW;
	
	m_bresponse = is_response;
	m_bchunked = 0;
	m_bmiss = 0;
	m_boriginal_head = 0;
	m_boriginal_body = 0;
	
	/*head*/
	run_chain_link_init(&m_head_buf_chain);
	m_head_last_buf_tail_len = 0;
	
	run_buf_init(&m_content_buf);
	
	memset(&m_chunk_ctx, 0, sizeof(http_chunked_ctx_t));
	run_chain_link_init(&m_chunk_offset_chain_link);
	run_chain_link_init(&m_chunk_frag_chain_link);
}

void CHTTPReassemble::close()
{
	if (!m_boriginal_head
		&& m_head.data != NULL){
		run_free(m_head.data);
		m_head.data = NULL;
	}
	else
	{
		m_head.data = NULL;
	}
	
	if (!m_boriginal_body
		&& m_body.data != NULL){
		run_free(m_body.data);
		m_body.data = NULL;
	}
	else
	{
		m_body.data = NULL;
	}
	
	/*缓存需要清除*/
	run_buf_destroy_space(&m_content_buf);
	run_chain_link_deinit(&m_head_buf_chain);
	run_chain_link_deinit(&m_chunk_offset_chain_link);
	run_chain_link_deinit(&m_chunk_frag_chain_link);
}

/*===================================================================
* 函数名称:  CHTTPReassemble::add_pkt_process
* 参数信息:  unsigned char *buf [IN]报文指针
			 uint32_t buf_len   [IN]报文长度
			 uint32_t seq_num   [IN]seq num
* 函数返回:  0为成功，<0 为失败
* 函数功能:  向当前重组对象加入报文
* 函数说明:  主要进行报文校验及重组处理
* 创建日期:  2016-12-08
* 修改人员： tianhuimeng
====================================================================*/
int CHTTPReassemble::add_pkt_process(unsigned char*buf, uint32_t buf_len, uint32_t seq_num)
{
	int ret;
	
	if (buf == NULL || buf_len == 0){
		return -1;
	}
	
	ret = check_process(buf, buf_len, seq_num);
	if (ret < 0){
		return -1;
	}
	
	process_after_check(buf, buf_len);
	return 0;
}

/*===================================================================
* 函数名称:  CHTTPDecode::force_reassemble
* 参数信息:  无
* 函数返回:  0为成功，<0 为失败
* 函数功能:  强制当前重组对象结束处理
* 函数说明:  强制当前缓存数据进行拼接等处理，重组对象最终状态为 STATUS_BODY_DONE
* 创建日期:  2016-12-08
* 修改人员： tianhuimeng
====================================================================*/
int CHTTPReassemble::force_reassemble()
{
	/*头部如果不完整，则直接reset*/
	if (get_status() < STATUS_BODY_START){
		reset();
		/*强制置为结束状态*/
		set_status(STATUS_BODY_DONE);
		return 0;
	}
	
	/*如果已经是完成状态，则直接返回即可,仅需要对body不完整做处理*/
	if (get_status() == STATUS_BODY_START){
		force_reassemble_body();
	}
	return 0;
}

/*===================================================================
* 函数名称:  CHTTPReassemble::is_done
* 参数信息:  无
* 函数返回:  1为完成，0 为未完成
* 函数功能:  当前重组对象是否完成处理
* 函数说明:  根据状态进行判断
* 创建日期:  2016-12-08
* 修改人员： tianhuimeng
====================================================================*/
int CHTTPReassemble::is_done()
{
	if (get_status() == STATUS_BODY_DONE){
		return 1;
	}
	return 0;
}

/*===================================================================
* 函数名称:  CHTTPReassemble::is_idle
* 参数信息:  无
* 函数返回:  1为空闲态，0 为非空闲
* 函数功能:  当前重组对象是否空闲
* 函数说明:  根据状态进行判断
* 创建日期:  2016-12-08
* 修改人员： tianhuimeng
====================================================================*/
int CHTTPReassemble::is_idle()
{
	if (get_status() == STATUS_INIT){
		return 1;
	}
	return 0;
}

/*===================================================================
* 函数名称:  CHTTPReassemble::notify_process_later
* 参数信息:  无
* 函数返回:  无
* 函数功能:  当前报文无法及时处理
* 函数说明:  主要用于将保存的原始报文转存
* 创建日期:  2016-12-08
* 修改人员： tianhuimeng
====================================================================*/
void CHTTPReassemble::notify_process_later()
{
	undo_head_original();
	undo_body_original();
	return;
}
/*=========================private function implement=============================================*/

int CHTTPReassemble::check_process(unsigned char *buf, uint32_t buf_len, uint32_t seq_num)
{	
	int pkt_type;
	
	switch (m_status)
	{
		case STATUS_INIT:
		{
			/*此包的首片，获取报文类型，如果报文类型有效则校验成功，否则校验失败*/
			pkt_type = http_get_pkt_type(buf, buf_len);
			if (is_valid_pkt_type(pkt_type)){
				/*校验成功，则置位status状态为 STATUS_HEAD_START,存储 exp_seq.*/
				set_status(STATUS_HEAD_START);
				save_exp_seq(seq_num+buf_len);
				return 0;
			}
			else{
				/*校验失败，则认为头部存在缺失，直接返回。（不再继续处理）*/
				//set_pkt_error();
				return -1;
			}
			break;
		}	
		case STATUS_HEAD_START:
		{
			/*根据当前包的seq与存储的exp_seq进行比较。*/
			if (check_seq_ok(seq_num)){
				/*校验成功，则存储exp_seq,以用作下一片包校验*/
				save_exp_seq(seq_num+buf_len);
				return 0;
			}
			else {
				/*校验失败，则认为存在分片缺失，不再继续处理,需要执行reset操作，并强制结束*/
				reset();
				set_pkt_error();
				set_status(STATUS_BODY_DONE);
				return -1;
			}
			break;
		}
			
		case STATUS_BODY_START:
		{
			/*根据当前包的seq与存储的exp_seq进行比较。*/
			if (check_seq_ok(seq_num)){
				/*校验成功，则存储exp_seq,以用作下一片包校验。*/
				save_exp_seq(seq_num+buf_len);
				return 0;
			}
			else {
				/*校验失败，则认为存在分片缺失,置状态为 STATUS_BODY_DONE。*/
				set_incomplete();
				force_reassemble();
				set_status(STATUS_BODY_DONE);
				return 0;
			}
			break;	
		}
		case STATUS_BODY_DONE:
			/*可能是重传包或者乱序严重时被认为丢包的那片包又过来了*/
			break;	
			
		default:
			//impossible
			assert(0);
			break;
	}
	return -1;
}


int CHTTPReassemble::head_tmp_buf_add(unsigned char *buf, uint32_t buf_len)
{
	run_chain_buf_t *chain_buf;
	
	chain_buf = run_create_chain_buf(buf_len);
	if (chain_buf == NULL){
		return -1;
	}
	
	run_buf_fill(chain_buf->buf, buf, buf_len);
	run_chain_link_add_buf(&m_head_buf_chain, chain_buf);
	return 0;
}

int head_tmp_buf_cb(run_chain_buf_t *chain_buf, void *priv)
{
	if (chain_buf == NULL || chain_buf->buf == NULL){
		return -1;
	}
	
	run_buf_t *buf = chain_buf->buf;
	
	run_buf_t *final_head = (run_buf_t *)priv;
	if (final_head == NULL){
		return -1;
	}
	
	run_buf_fill(final_head, buf->start, run_buf_get_len(buf));
	return 0;
}

int CHTTPReassemble::head_tmp_buf_assemble(unsigned char *last_buf, uint32_t last_buf_len)
{
	run_buf_t *final_head;
	int final_head_len;
	
	final_head_len = run_chain_link_get_total_len(&m_head_buf_chain) + last_buf_len;
	final_head = run_buf_create(final_head_len);
	if (final_head == NULL){
		return -1;
	}
	
	run_chain_link_loop(&m_head_buf_chain, head_tmp_buf_cb, final_head);
	run_buf_fill(final_head, last_buf,last_buf_len);
	
	/*将final_head下的内存块挂到m_head下，释放final_head*/
	m_head = run_buf_dispart_memory(final_head);
	run_buf_destroy(final_head);
	
	/*清除缓存*/
	run_chain_link_deinit(&m_head_buf_chain);
	
	return 0;
}


int CHTTPReassemble::head_start_process(unsigned char *buf, uint32_t buf_len, run_str_t & rest)
{
	unsigned char *head_end;
	uint32_t cur_head_len;
	int ret;
	
	rest.data = NULL;
	rest.len = 0;
	
	/*查找头部结束标记*/
	head_end = get_head_end(buf, buf_len);
	if (head_end != NULL){
		/* 头部已完整*/
		cur_head_len = head_end - buf;
		assert(buf_len >= cur_head_len);
		rest.len = buf_len - cur_head_len;
		if (rest.len != 0){
			rest.data = buf + cur_head_len;
		}
		
		if (is_head_tmp_buf_empty()){
			/*没有数据缓存，则当前片即包含了完整头部，暂不进行内存拷贝，head_buffer指向当前原始报文，并记标记*/
			m_head.data = buf;
			m_head.len = cur_head_len;
			set_head_original();
		}
		else {
			/*已有数据缓存，需要将已有数据缓存及当前分片组装完整*/
			ret = head_tmp_buf_assemble(buf, cur_head_len);
			if (ret < 0){
				set_inter_error();
				return -1;
			}
		}
		
		set_status(STATUS_HEAD_DONE);
	}
	else {
		/*不完整，需要将该片数据拷贝或拼接到 head 缓存中并退出处理*/
		head_tmp_buf_add(buf, buf_len); 
		/*防止头部结束标记被分割到两片或多片报文中，需要做特殊处理*/
		save_head_buf_tail(buf, buf_len);
	}
	return 0;
}


void CHTTPReassemble::save_head_buf_tail(unsigned char *buf, int buf_len)
{
	unsigned char *buf_tail = buf + buf_len;
	unsigned char ch;
	uint32_t save_tail_len = 0;
	char tmp_buf_tail[HTTP_HEAD_END_FLAG_LEN];
	uint32_t i;
	
	/*考虑极端情况，上一片已有缓存尾部，当前片只有少于两个字符，可能需要继续拼接*/
	if (buf_len == 1 && (*buf == 0x0d || *buf == 0x0a)
		&& m_head_last_buf_tail_len > 0 && m_head_last_buf_tail_len < 3)
	{
		if (m_head_last_buf_tail[m_head_last_buf_tail_len -1] != *buf)	{
			m_head_last_buf_tail[m_head_last_buf_tail_len++] = *buf;
			return;
		}
	}
	
	if (buf_len == 2 && (*buf == 0x0d || *buf == 0x0a) && (*(buf+1) == 0x0d || *(buf+1) == 0x0a) && (*buf != *(buf+1))
		&& m_head_last_buf_tail_len == 1)
	{
		if (m_head_last_buf_tail[m_head_last_buf_tail_len -1] != *buf)	{
			m_head_last_buf_tail[m_head_last_buf_tail_len++] = *buf;
			m_head_last_buf_tail[m_head_last_buf_tail_len++] = *(buf+1);
			return;
		}
	}
	
	/*考虑头部结束标记被分割到两片报文中的情况，仅需要最多检查最后几个字节*/
	for (i = 0; i < HTTP_HEAD_END_FLAG_LEN-1; i++){	
		if (buf_tail-1-i < buf){
			break;
		}
		
		ch = *(buf_tail-1-i);
		
		if ((ch == '\r') || (ch == '\n')){
			/*必须是\r\n交叉出现的*/
			if (i > 0 && (tmp_buf_tail[i-1] == ch)){
				break;
			}
			else{
				save_tail_len++;
				tmp_buf_tail[i] = ch;
			}
		}
		else{
			break;
		}
	}
	
	if (save_tail_len == 0){
		return;
	}
	
	/*再次检查,必须以0x0d开头*/
	if (tmp_buf_tail[save_tail_len-1] != 0x0d){
		save_tail_len--;
	}
	
	/*需要翻转过来，进行保存*/
	for (i = 0; i < save_tail_len; i++){
		m_head_last_buf_tail[i] = tmp_buf_tail[save_tail_len-1-i];
	}
	m_head_last_buf_tail_len = save_tail_len;
	
	return;
}

unsigned char * CHTTPReassemble::get_head_with_last_buf_tail(unsigned char *buf, unsigned char *buf_end, unsigned char *first_br_pos)
{
	unsigned char *p = first_br_pos;
	if (p == buf)
	{
		/*需要检查缓存的上一片尾部*/
		if (m_head_last_buf_tail_len == HTTP_HEAD_END_FLAG_LEN - 1
			&& m_head_last_buf_tail[m_head_last_buf_tail_len - 1] == 0x0d)
		{
			/*完整，返回指针（跳过头结束）*/
			return (p+1);
		}
		else if (m_head_last_buf_tail_len == 1
			&& m_head_last_buf_tail[m_head_last_buf_tail_len - 1] == 0x0d)
		{
			if (p + 2 < buf_end){
				if ( *(p+1) == 0x0d && *(p+2) == 0x0a){
					/*完整，返回指针（跳过头结束）*/
					return (p+3);
				}
			}
			else {
				/*需要考虑继续存储，由存储函数处理*/
				return NULL;
			}
		}
		else {
			/*需要继续查找,缓存的没用了，需要清除掉*/
			m_head_last_buf_tail_len = 0;
		}
	}
	else if (p == buf+1){
		if (*(p-1) == 0x0d
			&& m_head_last_buf_tail_len == HTTP_HEAD_END_FLAG_LEN - 2
			&& m_head_last_buf_tail[m_head_last_buf_tail_len - 1] == 0x0a)
		{
			/*完整，返回指针（跳过头结束）*/
			return (p+1);
		}
		else{
			/*需要继续查找,缓存的没用了，需要清除掉*/
			m_head_last_buf_tail_len = 0;
		}
	}
	else {
		/*不会存在拼接，需要清除掉缓存的尾部*/
		m_head_last_buf_tail_len = 0;
	}
	return NULL;
}

unsigned char *CHTTPReassemble::get_head_end(unsigned char *buf, int buf_len)
{
	unsigned char *p;
	unsigned char *buf_pos = buf;
	unsigned char *buf_end = buf + buf_len;
	unsigned char *head_pos;
	int first_br = 1;
	
	while (buf_pos < buf_end){
		p = (unsigned char *)memchr(buf_pos, 0x0a, buf_end-buf_pos);
		if (p == NULL){
			break;
		}
		
		/*本片第一个换行符需要考虑到头部结束可能被分片的可能，先进行该部分处理*/
		if (first_br){
			head_pos = get_head_with_last_buf_tail(buf, buf_end, p);
			if (head_pos != NULL){
				return head_pos;
			}
			first_br = 0;
		}
		
		if (p-1>=buf
			&& *(p-1) == 0x0d
			&& p+2 < buf_end
			&& *(p+1) == 0x0d && *(p+2) == 0x0a)
		{
			return p+3;
		}
		buf_pos = p+1;
	}
	return NULL;
}

int CHTTPReassemble::undo_head_original()
{
	unsigned char *tmp;
	
	if (m_boriginal_head ==0){
		return 0;
	}
	
	assert(m_head.data != NULL && m_head.len != 0);
	
	tmp = (unsigned char *)run_malloc(MID_HTTP_REASSEMBLE, m_head.len);
	if (tmp != NULL){
		memcpy(tmp, m_head.data, m_head.len);
		m_head.data = tmp;
		m_boriginal_head = 0;
		return 0;
	}
	else{
		/*out of mem，需要清除掉head信息*/
		m_head.data = NULL;
		m_head.len = 0;
	}
	return -1;
}

int CHTTPReassemble::undo_body_original()
{
	unsigned char *tmp;
	
	if (m_boriginal_body ==0){
		return 0;
	}
	
	assert(m_body.data != NULL && m_body.len != 0);
	
	tmp = (unsigned char *)run_malloc(MID_HTTP_REASSEMBLE, m_body.len);
	if (tmp != NULL){
		memcpy(tmp, m_body.data, m_body.len);
		m_body.data = tmp;
		m_boriginal_body = 0;
		return 0;
	}
	else{
		/*out of mem，需要清除掉body信息*/
		m_body.data = NULL;
		m_body.len = 0;
	}
	return -1;
}


int CHTTPReassemble::pre_process_header()
{	
	static const unsigned char s_content_length_header_name[] = "content-length: ";
	static const unsigned char s_transfer_encoding_header_name[] = "transfer-encoding: ";
	static const uint32_t s_cl_header_name_len = 16;
	static const uint32_t s_te_header_name_len = 19;
	
	unsigned char *buf = m_head.data;
	uint32_t buf_len = m_head.len;
	
	if (buf == NULL || buf_len == 0){
		return -1;
	}
	
	unsigned char ch;
	unsigned char *p;
	unsigned char *buf_pos = buf;
	unsigned char *buf_end = buf + buf_len;
	
	while(buf_pos < buf_end){
		p = (unsigned char *)memchr(buf_pos, 0x0a, buf_end-buf_pos);
		if (p == NULL){
			break;
		}
		
		uint32_t line_len = p-1 - buf_pos; //需要跳过0x0d
		ch = *buf_pos;
		
		if (line_len > s_cl_header_name_len && (ch == 'c' || ch == 'C')){
			/*Content-Length:  */
			if (run_strncasecmp(buf_pos, (unsigned char *)s_content_length_header_name, s_cl_header_name_len) == 0){
				buf_pos += s_cl_header_name_len;
				uint32_t len = (uint32_t)run_atoof(buf_pos, p-1-buf_pos);
				set_content_len(len);
				return 0;
			}
		}
		else if (line_len >= (s_te_header_name_len+7) && (ch == 't' || ch == 'T')){
			/*Transfer-Encoding: */
			if (run_strncasecmp(buf_pos, (unsigned char *)s_transfer_encoding_header_name, s_te_header_name_len) == 0){
				buf_pos += s_te_header_name_len;
				
				if(run_strncasecmp(buf_pos, (unsigned char *)"chunked", 7) == 0){
					set_chunked();
					/*获取到了高优先级信息，可以直接退出了*/
					return 0;
				}
			}
		}
		
		buf_pos = p+1;
	}	
	return 0;
}


int CHTTPReassemble::http_parse_chunked(run_buf_t *b, http_chunked_ctx_t *ctx)
{
    unsigned char *pos, ch, c;
    int   rc;
    enum sw_state{
        sw_chunk_start = 0,
        sw_chunk_size,
        sw_chunk_extension,
        sw_chunk_extension_almost_done,
        sw_chunk_data,
        sw_after_data,
        sw_after_data_almost_done,
        sw_last_chunk_extension,
        sw_last_chunk_extension_almost_done,
        sw_trailer,
        sw_trailer_almost_done,
        sw_trailer_header,
        sw_trailer_header_almost_done
    };

    uint32_t state = ctx->state;

    if (state == sw_chunk_data && ctx->size == 0) {
        state = sw_after_data;
    }

    rc = RUN_AGAIN;

    for (pos = b->pos; pos < b->last; pos++) {

        ch = *pos;

        switch (state) {

        case sw_chunk_start:
            if (ch >= '0' && ch <= '9') {
                state = sw_chunk_size;
                ctx->size = ch - '0';
                break;
            }

            c = (u_char) (ch | 0x20);

            if (c >= 'a' && c <= 'f') {
                state = sw_chunk_size;
                ctx->size = c - 'a' + 10;
                break;
            }

            goto invalid;

        case sw_chunk_size:
            if (ctx->size > RUN_MAX_OFF_UINT32_VALUE / 16) {
                goto invalid;
            }

            if (ch >= '0' && ch <= '9') {
                ctx->size = ctx->size * 16 + (ch - '0');
                break;
            }

            c = (u_char) (ch | 0x20);

            if (c >= 'a' && c <= 'f') {
                ctx->size = ctx->size * 16 + (c - 'a' + 10);
                break;
            }

            if (ctx->size == 0) {

                switch (ch) {
                case CR:
                    state = sw_last_chunk_extension_almost_done;
                    break;
                case LF:
                    state = sw_trailer;
                    break;
                case ';':
                case ' ':
                case '\t':
                    state = sw_last_chunk_extension;
                    break;
                default:
                    goto invalid;
                }

                break;
            }

            switch (ch) {
            case CR:
                state = sw_chunk_extension_almost_done;
                break;
            case LF:
                state = sw_chunk_data;
                break;
            case ';':
            case ' ':
            case '\t':
                state = sw_chunk_extension;
                break;
            default:
                goto invalid;
            }

            break;

        case sw_chunk_extension:
            switch (ch) {
            case CR:
                state = sw_chunk_extension_almost_done;
                break;
            case LF:
                state = sw_chunk_data;
            }
            break;

        case sw_chunk_extension_almost_done:
            if (ch == LF) {
                state = sw_chunk_data;
                break;
            }
            goto invalid;

        case sw_chunk_data:
            rc = RUN_OK;
            goto data;

        case sw_after_data:
            switch (ch) {
            case CR:
                state = sw_after_data_almost_done;
                break;
            case LF:
                state = sw_chunk_start;
            }
            break;

        case sw_after_data_almost_done:
            if (ch == LF) {
                state = sw_chunk_start;
                break;
            }
            goto invalid;

        case sw_last_chunk_extension:
            switch (ch) {
            case CR:
                state = sw_last_chunk_extension_almost_done;
                break;
            case LF:
                state = sw_trailer;
            }
            break;

        case sw_last_chunk_extension_almost_done:
            if (ch == LF) {
                state = sw_trailer;
                break;
            }
            goto invalid;

        case sw_trailer:
            switch (ch) {
            case CR:
                state = sw_trailer_almost_done;
                break;
            case LF:
                goto done;
            default:
                state = sw_trailer_header;
            }
            break;

        case sw_trailer_almost_done:
            if (ch == LF) {
                goto done;
            }
            goto invalid;

        case sw_trailer_header:
            switch (ch) {
            case CR:
                state = sw_trailer_header_almost_done;
                break;
            case LF:
                state = sw_trailer;
            }
            break;

        case sw_trailer_header_almost_done:
            if (ch == LF) {
                state = sw_trailer;
                break;
            }
            goto invalid;

        }
    }

data:

    ctx->state = state;
    b->pos = pos;

    if (ctx->size > RUN_MAX_OFF_UINT32_VALUE - 5) {
        goto invalid;
    }
#if 0
    switch (state) {

    case sw_chunk_start:
        ctx->length = 3 /* "0" LF LF */;
        break;
    case sw_chunk_size:
        ctx->length = 1 /* LF */
                      + (ctx->size ? ctx->size + 4 /* LF "0" LF LF */
                                   : 1 /* LF */);
        break;
    case sw_chunk_extension:
    case sw_chunk_extension_almost_done:
        ctx->length = 1 /* LF */ + ctx->size + 4 /* LF "0" LF LF */;
        break;
    case sw_chunk_data:
        ctx->length = ctx->size + 4 /* LF "0" LF LF */;
        break;
    case sw_after_data:
    case sw_after_data_almost_done:
        ctx->length = 4 /* LF "0" LF LF */;
        break;
    case sw_last_chunk_extension:
    case sw_last_chunk_extension_almost_done:
        ctx->length = 2 /* LF LF */;
        break;
    case sw_trailer:
    case sw_trailer_almost_done:
        ctx->length = 1 /* LF */;
        break;
    case sw_trailer_header:
    case sw_trailer_header_almost_done:
        ctx->length = 2 /* LF LF */;
        break;

    }
#endif
    return rc;

done:

    ctx->state = 0;
    b->pos = pos + 1;

    return RUN_DONE;

invalid:

    return RUN_ERROR;
}

int CHTTPReassemble::save_chunk_offset(run_buf_t *org_buf, unsigned char *chunk_start, unsigned char *chunk_end)
{
	run_chain_buf_t *chain_buf;
	run_buf_t *b;
	
	/*create a chain buf*/
	chain_buf = run_create_chain_buf(0);
	if (chain_buf == NULL) {
		set_inter_error();
		return -1;
	}

	b = chain_buf->buf;
			
	/*start can be used to calc the offset and check valid*/
	b->start = org_buf->start;
	/*end is useless*/
	b->end = org_buf->end;
	
	b->pos = chunk_start;
	b->last = chunk_end;
	b->read_only = 1;
	/*for check*/
	b->index = m_chunk_ctx.frag_num;
	m_chunk_ctx.content_len += (chunk_end - chunk_start);
	
	run_chain_link_add_buf(&m_chunk_offset_chain_link, chain_buf);
	return 0;		
}

int CHTTPReassemble::save_chunk_frag_org(unsigned char *buf, uint32_t buf_len)
{
	run_chain_buf_t *chain_buf;
	run_buf_t *b;
	
	chain_buf = run_create_chain_buf(0);
	if (chain_buf == NULL) {
		set_inter_error();
		return -1;
	}
	
	b = chain_buf->buf;				
	run_buf_init_assign(b, buf, buf_len);			
	b->index = m_chunk_ctx.frag_num;
	
	run_chain_link_add_buf(&m_chunk_frag_chain_link, chain_buf);
	return 0;
}

int CHTTPReassemble::save_chunk_frag_cpy(unsigned char *buf, uint32_t buf_len)
{
	run_chain_buf_t *chain_buf;
	run_buf_t *b;
	
	chain_buf = run_create_chain_buf(buf_len);
	if (chain_buf == NULL) {
		set_inter_error();
		return -1;
	}
	
	b = chain_buf->buf;
	b->index = m_chunk_ctx.frag_num;
	run_buf_fill(b, buf, buf_len);	
	run_chain_link_add_buf(&m_chunk_frag_chain_link, chain_buf);
	return 0;
}


int CHTTPReassemble::chunk_frag_process(unsigned char *buf, uint32_t buf_len)
{
	int rc;
	
	if (buf == NULL || buf_len == 0){
		/*对于当前长度为0的，明显报文不完整，返回前需要考虑对头部的处理*/
		undo_head_original();
		return 0;
	}
	
	run_buf_t input;
	run_chain_buf_t *chain_buf;
	run_buf_t *b;
	
	run_buf_init_assign(&input, buf, buf_len);
	
	/*for check*/
	m_chunk_ctx.frag_num++;
	
	for ( ;; ) {

		rc = http_parse_chunked(&input, &m_chunk_ctx);

		if (rc == RUN_OK) {
			/* a chunk has been parsed successfully */
			unsigned char *start_pos = input.pos;
			uint32_t size = input.last - input.pos;
			uint32_t cur_chunk_len;
			
			if (size > m_chunk_ctx.size) {
				cur_chunk_len = m_chunk_ctx.size;
				m_chunk_ctx.size = 0;
			}
			else {
				cur_chunk_len = size;
				m_chunk_ctx.size -= size;			
			}
			/*需要偏移，后续继续处理*/
			input.pos += cur_chunk_len;
			
			save_chunk_offset(&input, start_pos, start_pos + cur_chunk_len);
					
			if (g_max_body_buf_len <= m_chunk_ctx.content_len)
			{
				save_chunk_frag_org(buf, buf_len);
				set_incomplete();
				chunk_assemble();
				set_status(STATUS_BODY_DONE);
				return 0;
			}
			continue;
		}

		if (rc == RUN_DONE) {
			/* a whole response has been parsed successfully */
			/*trailer不需要再处理*/	
			
			save_chunk_frag_org(buf, buf_len);	
			chunk_assemble();
			set_status(STATUS_BODY_DONE);
			break;
		}

		if (rc == RUN_AGAIN) {
			/*data不完整，相当于将这个data又切片了，剩余待收取部分记录在m_chunk_ctx中*/
			/*需要拷贝内存*/
			save_chunk_frag_cpy(buf, buf_len);
			undo_head_original();
			break;
		}

		/* invalid */
		set_pkt_error();
		return -1;
	}
	
	return 0;
}


int CHTTPReassemble::body_frag_process(unsigned char *buf, uint32_t buf_len)
{
	int ret;
	uint32_t len;
	
	/*content-len格式*/
	if (m_content_len > 0){
		if (run_buf_is_empty(&m_content_buf)){
			/*当前没有缓存body*/			
			if (is_cur_content_body_complete(buf_len)){
				/*body 完整,暂不另外申请内存，指向原始报文，需要置位*/
				set_body_original();
				set_status(STATUS_BODY_DONE);

				m_body.data = buf;
				//m_body.len = m_content_len; 
				if (m_content_len > g_max_body_buf_len){
					m_body.len = g_max_body_buf_len;
					set_incomplete();
				}
				else {
					m_body.len = m_content_len;
				}
			}
			else {
				uint32_t content_buf_len;
				
				/*body 不完整，需要缓存*/	
				/*首先，头部还需要处理，如果存在 head_original 标记 则需要拷贝到头部缓存中，并清除该标记位*/
				undo_head_original();
					
				/*限制body存储大小（默认8M，可配置）*/
				if (m_content_len > g_max_body_buf_len){
					content_buf_len = g_max_body_buf_len;
					set_incomplete();
				}
				else {
					content_buf_len = m_content_len;
				}
				/*申请整块所需内存*/
				ret = run_buf_create_space(&m_content_buf, content_buf_len);
				if (ret == 0){
					/*将剩余数据加入到缓存*/
					if (buf_len > 0){
						run_buf_fill(&m_content_buf, buf, buf_len);
					}
				}
				else {
					/*out of mem*/
					set_inter_error();
					return -1;
				}
			}
		}
		else{
			/*有body缓存,将当前片直接加入缓存，需要判断是否缓存已满，此时需要置status*/
			len = run_buf_fill_part(&m_content_buf, buf, buf_len);
			if (run_buf_is_full(&m_content_buf)){
				set_status(STATUS_BODY_DONE);
				m_body = run_buf_dispart_memory(&m_content_buf);
				run_buf_init(&m_content_buf);
			}
			else{
				assert(len == buf_len);
			}
		}
	}
	else if (m_bchunked) {
		/*进行 chunked 解析，计算各chunk累加长度*/
		chunk_frag_process(buf, buf_len);
	}
	else {
		//assume no body
		m_body.data = NULL;
		m_body.len = 0; 
		set_status(STATUS_BODY_DONE);
	}
	return 0;
}

int chunk_assemble_offset_cb(run_chain_buf_t *chain_buf, void *priv)
{
	if (chain_buf == NULL || chain_buf->buf == NULL){
		return -1;
	}
	
	chunk_assemble_param_t *assemble_param = (chunk_assemble_param_t *)priv;
	run_buf_t *frag_buf = assemble_param->chunk_frag_buf;
	run_buf_t *final_body = assemble_param->final_body;
	
	unsigned char *tmp_pos;
	unsigned char *tmp_last;
	
	run_buf_t *chunk_buf = chain_buf->buf;
	
	if (chunk_buf == NULL){
		return -1;
	}
	/*检查是否匹配*/
	if (frag_buf->index != chunk_buf->index){
		return 0;
	}
	
	if (frag_buf->start == chunk_buf->start){
		run_buf_fill_part(final_body, chunk_buf->pos, (chunk_buf->last - chunk_buf->pos));
	}
	else{
		tmp_pos = frag_buf->start + (chunk_buf->pos - chunk_buf->start);
		tmp_last = frag_buf->start + (chunk_buf->last - chunk_buf->start);
		assert(tmp_pos < tmp_last);
		run_buf_fill_part(final_body, tmp_pos, (tmp_last - tmp_pos));
	}
	return 0;
}

int chunk_assemble_frag_cb(run_chain_buf_t *chain_buf, void *priv)
{
	if (chain_buf == NULL || chain_buf->buf == NULL){
		return -1;
	}
	
	chunk_assemble_param_t *assemble_param = (chunk_assemble_param_t *)priv;
	assemble_param->chunk_frag_buf = chain_buf->buf;
	
	run_chain_link_loop(assemble_param->chunk_offset_chain_link, chunk_assemble_offset_cb, assemble_param);
	return 0;
}

int CHTTPReassemble::chunk_assemble()
{
	run_buf_t *final_body;
	
	uint32_t min_len = run_min(m_chunk_ctx.content_len, g_max_body_buf_len);
	final_body = run_buf_create(min_len);
	if (final_body == NULL){
		return -1;
	}
	
	chunk_assemble_param_t assemble_param;
	memset(&assemble_param, 0, sizeof(chunk_assemble_param_t));
	assemble_param.final_body = final_body;
	assemble_param.chunk_offset_chain_link = &m_chunk_offset_chain_link;
	//assemble_param.chunk_offset_buf = m_chunk_offset_chain_link.head;
	
	run_chain_link_loop(&m_chunk_frag_chain_link, chunk_assemble_frag_cb, &assemble_param);
	
	m_body = run_buf_dispart_memory(final_body);
	run_buf_destroy(final_body);
	return 0;
}


int CHTTPReassemble::force_reassemble_body()
{
	if (m_body.data != NULL && m_body.len != 0){
		return 0;
	}
	
	/*content-len格式*/
	if (m_content_len > 0){
		if (run_buf_is_empty(&m_content_buf)){
			/*当前没有缓存body*/
		}
		else{
			m_body = run_buf_dispart_memory(&m_content_buf);
			run_buf_init(&m_content_buf);
		}
	}
	else if (m_bchunked) {
		chunk_assemble();
	}
	else {
		//assume no body
	}
	set_status(STATUS_BODY_DONE);
	return 0;
}


int CHTTPReassemble::process_after_check(unsigned char *buf, uint32_t buf_len)
{
	int ret;
	unsigned char *head_end = NULL;
	run_str_t rest;
	
	switch (m_status){
		case STATUS_HEAD_START:
			ret = head_start_process(buf, buf_len, rest);
			if (ret != 0){
				return -1;
			}
			
			if (get_status() != STATUS_HEAD_DONE){
				return 0;
			}
			
			/*m_head已经完整，预解析头部，Content-Length 、Transfer-Encoding 头部，获取到 content_len 或 is_chunked 标记。*/
			ret = pre_process_header();
			if (ret < 0){
				/*impossible, just in case*/
				set_inter_error();
				return -1;
			}
			
			/*剩余的body部分处理由专门case处理.即使当前rest实际为0也需要进入处理*/
			set_status(STATUS_BODY_START);
			buf = rest.data;
			buf_len = rest.len;
			//break;
			
		case STATUS_BODY_START:	
			ret = body_frag_process(buf, buf_len);
			if (ret < 0){
				return -1;
			}
			
			if (get_status() != STATUS_BODY_DONE){
				return 0;
			}
			break;
		
		case STATUS_BODY_DONE:	
			//do nothing
			break;
			
		default:
			/*impossible*/
			return -1;
	}
	return 0;
}

