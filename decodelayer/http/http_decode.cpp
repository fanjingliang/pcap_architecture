/********************************************************************
* 文件名称：http_decode.cpp
* 创建人员：tianhuimeng
* 创建日期：2016-12-15
* 文件描述：http基础解码，管理请求、响应报文重组、解析等
*********************************************************************/

#include "http_decode.h"
#include "run_base.h"
#include "run_gzip.h"
#include "run_deflate.h"
/*===================================================================
* 函数名称:  CHTTPDecode::add_pkt
* 参数信息:  unsigned char *pkt [IN]报文指针
			 uint32_t pkt_len   [IN]报文长度
			 int direction      [IN]方向(DIRECTION_CLIENT_TO_SERVER/DIRECTION_SERVER_TO_CLIENT)
			 uint32_t seq       [IN]报文seq num
* 函数返回:  0为成功，<0 为失败
* 函数功能:  解码对象中增加报文
* 函数说明:  主要调用重组对象进行报文重组，如果完整则调用解析对象进行解析处理
* 创建日期:  2016-12-15
* 修改人员： tianhuimeng
====================================================================*/
int CHTTPDecode::add_pkt(unsigned char *pkt, uint32_t pkt_len, int direction, uint32_t seq)
{	
	run_str_t head;
	run_str_t body;
	CHTTPReassemble *reassemble_obj;
	CHttpPacket *http_pkt;
		
	run_str_null(&head);
	run_str_null(&body);

	//printf("httpdecode add pkt[seq:%x, len:%d]\n",seq, pkt_len);

	if (direction == DIRECTION_CLIENT_TO_SERVER){
		reassemble_obj = &m_request_reassemble;
	}
	else {
		reassemble_obj = &m_response_reassemble;
	}
	reassemble_obj->add_pkt_process(pkt, pkt_len, seq);
	if (reassemble_obj->is_done()){
		//printf("\ndirection[%d] pkt is done\n", direction);
	}
	else {
		return 0;
	}

	/*for test start-- delete later*/
#if 0
	static unsigned char buf[2048];
	uint32_t max_buf_len = 2048-1;
	uint32_t buf_len;
	head = reassemble_obj->get_head();
	body = reassemble_obj->get_body();

	buf_len = run_min(head.len, max_buf_len);
	if (head.len != 0){
		run_snprintf(buf, buf_len, "%s",head.data);
	}
	buf[buf_len] = '\0';	
	printf("head[len:%lu] is :\n%s\n",head.len, buf);
	
	buf_len = run_min(body.len, max_buf_len);
	if (body.len != 0){
		run_snprintf(buf, buf_len, "%s",body.data);
	}
	buf[buf_len] = '\0';
	//printf("body[len:%lu].\n",body.len);
#endif	
	/*for test end-- delete later*/
	
	if (!is_done()){
		reassemble_obj->notify_process_later();
		return 0;
	}	
	
	/*解析req and rsp */
	head = m_request_reassemble.get_head();
	body = m_request_reassemble.get_body();
	http_pkt = &m_request_parse;

	if (head.len != 0){
		http_pkt->http_head_parse(head.data, head.len);
	}
	if (body.len != 0){
		http_pkt->http_body_parse(body.data, body.len);
	}

	head = m_response_reassemble.get_head();
	body = m_response_reassemble.get_body();
	http_pkt = &m_response_parse;

	if (head.len != 0){
		http_pkt->http_head_parse(head.data, head.len);
	}
	if (body.len != 0){
		http_pkt->http_body_parse(body.data, body.len);
	}

	http_preprocess_headers();
	http_preprocess_body();
	
	return 0;
}


/*===================================================================
* 函数名称:  CHTTPDecode::is_done
* 参数信息:  int has_ack [IN]是否有下行数据
* 函数返回:  1为已完成，0 为未完成
* 函数功能:  解码对象是否已完成处理
* 函数说明:  主要根据重组对象的状态进行判断
* 创建日期:  2016-12-15
* 修改人员： tianhuimeng
====================================================================*/
int CHTTPDecode::is_done()
{
	if (m_response_reassemble.is_done()
		&& (!m_bhas_ack || m_response_reassemble.is_done()))
	{
		return 1;
	}
	return 0;
}

/*===================================================================
* 函数名称:  CHTTPDecode::force_reassemble
* 参数信息:  无
* 函数返回:  0为成功，<0 为失败
* 函数功能:  强制当前解码对象结束处理
* 函数说明:  主要调用重组对象进行报文重组，并调用解析处理
* 创建日期:  2016-12-15
* 修改人员： tianhuimeng
====================================================================*/
int CHTTPDecode::force_reassemble()
{
	run_str_t head;
	run_str_t body;
	
	m_request_reassemble.force_reassemble();
	m_response_reassemble.force_reassemble();
	
	//后续解析
	head = m_request_reassemble.get_head();
	body = m_request_reassemble.get_body();
	
	/*解析*/
	if (head.len != 0){
		m_request_parse.http_head_parse(head.data, head.len);
	}
	if (body.len != 0){
		m_request_parse.http_body_parse(body.data, body.len);
	}

	head = m_response_reassemble.get_head();
	body = m_response_reassemble.get_body();
	
	/*解析*/
	if (head.len != 0){
		m_response_parse.http_head_parse(head.data, head.len);
	}
	if (body.len != 0){
		m_response_parse.http_body_parse(body.data, body.len);
	}

	http_preprocess_headers();
	http_preprocess_body();

}

/*===================================================================
* 函数名称:  CHTTPDecode::is_mine
* 参数信息:  unsigned char *pkt [IN]报文指针
			 uint32_t pkt_len   [IN]报文长度
			 int direction      [IN]方向(DIRECTION_CLIENT_TO_SERVER/DIRECTION_SERVER_TO_CLIENT)
* 函数返回:  1为当前报文属于本解码对象，0 为不属于
* 函数功能:  判断当前报文是否属于本解码对象
* 函数说明:  主要根据是否为新的req/rsp以及重组对象当前状态进行判断
* 创建日期:  2016-12-15
* 修改人员： tianhuimeng
====================================================================*/
int CHTTPDecode::is_mine(unsigned char *pkt, uint32_t pkt_len, int direction)
{
	int pkt_type;
	
	/*获取pkt type,首先确认是否为新的request或response到来*/
	pkt_type = http_get_pkt_type(pkt, pkt_len);
	if (pkt_type == HTTP_PKT_TYPE_UNKNOW){
		/*不是新的req或rsp,则默认为属于当前解码对象（前面已经排过序了）*/
		return 1;
	}
	
	/*需要判断是否为idle状态*/
	if (direction == DIRECTION_CLIENT_TO_SERVER){
		if (m_request_reassemble.is_idle()
			&& m_response_reassemble.is_idle())
		{
			return 1;
		}
	}
	else {
		if (m_response_reassemble.is_idle())
		{
			return 1;
		}
	}
	return 0;
}

void CHTTPDecode::init(uint32_t has_ack)
{
	m_bhas_ack = has_ack;

	memset(&m_req_body, 0, sizeof(http_body_t));
	memset(&m_rsp_body, 0, sizeof(http_body_t));
	run_str_null(&m_url);
}

void CHTTPDecode::close()
{
	if (m_req_body.is_newalloc){
		run_free(m_req_body.body.data);
	}

	if (m_rsp_body.is_newalloc){
		run_free(m_rsp_body.body.data);
	}
	run_str_null(&(m_req_body.body));
	run_str_null(&(m_rsp_body.body));

	if (m_url.data != NULL){
		run_free(m_url.data);
	}
	run_str_null(&m_url);
	
}

int CHTTPDecode::http_get_zip_type(run_str_t &content_enc)
{
	int zip_type = HTTP_BODY_ZIP_NO;
	unsigned char *p;	

	if (content_enc.len == 0){
		return zip_type;
	}
	
	if ((p = run_strnstr ((unsigned char *)content_enc.data, (char *)"gzip", content_enc.len)) != NULL){
		zip_type = HTTP_BODY_ZIP_GZIP;
	}
	else if ((p = run_strnstr ((unsigned char *)content_enc.data, (char *)"deflate", content_enc.len)) != NULL){
		zip_type = HTTP_BODY_ZIP_DEFLATE;
	}
		
	return zip_type;
}

int CHTTPDecode::http_preprocess_headers()
{
	/*判断是否压缩*/
	run_str_t content_enc;

	content_enc = get_req_header(HTTP_HEADER_CONTENT_ENCODING);
	m_req_body.zip_type = http_get_zip_type(content_enc);

	content_enc = get_rsp_header(HTTP_HEADER_CONTENT_ENCODING);
	m_rsp_body.zip_type = http_get_zip_type(content_enc);


	/*获取字符集*/

	/*获取url*/
	process_url();
	
	return 0;
}

int CHTTPDecode::http_unzip(run_str_t &body, int zip_type)
{
	if (body.len == 0 || zip_type == HTTP_BODY_ZIP_NO){
		return -1;
	}
	
	int ret = -1;
	run_str_t tmp_dst;
	tmp_dst.len = body.len * 10;
	tmp_dst.data = (unsigned char *)run_malloc(MID_HTTPDECODE, tmp_dst.len);
	assert(tmp_dst.data != NULL);
	if (tmp_dst.data == NULL){
		return -1;
	}
	

	if (zip_type == HTTP_BODY_ZIP_GZIP){
		ret = run_gzip_uncompress(&tmp_dst, &body);
	}
	else if (zip_type == HTTP_BODY_ZIP_DEFLATE){
		ret = run_zlib_uncompress(&tmp_dst, &body);
	}

	if (ret != 0){
		run_free(tmp_dst.data);
		return -1;
	}

	body = tmp_dst;
	return 0;
}

int CHTTPDecode::http_preprocess_body()
{
	m_req_body.body = m_request_parse.get_body();
	m_req_body.is_complete = m_request_reassemble.is_body_complete();

	m_rsp_body.body = m_response_parse.get_body();
	m_rsp_body.is_complete = m_response_reassemble.is_body_complete();

#if 0
	/*首先判断body 是否完整*/
	int ret;
	if (m_req_body.is_complete){
		ret = http_unzip(m_req_body.body, m_req_body.zip_type);
		if (ret == 0){
			m_req_body.zip_type = HTTP_BODY_ZIP_NO;
			m_req_body.is_newalloc = 1;
			printf("req body unzip suc! unzip len:%lu\n", m_req_body.body.len);
		}
	}

	
	if (m_rsp_body.is_complete){
		ret = http_unzip(m_rsp_body.body, m_rsp_body.zip_type);
		if (ret == 0){
			m_rsp_body.zip_type = HTTP_BODY_ZIP_NO;
			m_rsp_body.is_newalloc = 1;
			printf("rsp body unzip suc! unzip len:%lu\n", m_rsp_body.body.len);
		}
	}
#endif
	
	return 0;
}

void CHTTPDecode::process_url()
{
	//get host
	run_str_t host = get_req_header(HTTP_HEADER_HOST);
	if (host.len == 0){
		return;
	}

	//skip space	
	unsigned char * host_start = host.data;
	unsigned char * host_end = (host.data + host.len);
	while (host_start < host_end && *host_start == ' '){
		host_start++;
	}
	if (host_start >= host_end){
		return;
	}

	//remove port
	unsigned char * tmp;
	tmp = run_strlchr(host_start, host_end, ':');
	if (tmp != NULL){
		host_end = tmp;
	}

	//get uri
	run_str_t uri = get_uri();
	if (uri.len == 0){
		return;
	}
	
	//skip space	
	unsigned char * uri_start = uri.data;
	unsigned char * uri_end = (uri.data + uri.len);
	while (uri_start < uri_end && *uri_start == ' '){
		uri_start++;
	}
	if (uri_start >= uri_end){
		return;
	}

	//remove version
	tmp = run_strnstr(uri_start, (char *)" HTTP/1.", uri.len);
	if (tmp != NULL){
		uri_end = tmp;
	}

	//combine
	uint32_t data_len = 0;
	unsigned char *data = NULL;
	if (*uri_start == '/'){
		data_len = (host_end - host_start) + (uri_end - uri_start);
		data = (unsigned char *)run_malloc(MID_HTTPDECODE, data_len+1);
		assert(data != NULL);
		memcpy(data, host_start, (host_end - host_start));
		memcpy(data+(host_end - host_start), uri_start, (uri_end - uri_start));
	}
	else {
		data_len = (uri_end - uri_start);
		data = (unsigned char *)run_malloc(MID_HTTPDECODE, data_len+1);
		assert(data != NULL);
		memcpy(data, uri_start, data_len);
	}
	//just incase
	data[data_len] = '\0';

	m_url.data = data;
	m_url.len = data_len;
	
	return ;
}

