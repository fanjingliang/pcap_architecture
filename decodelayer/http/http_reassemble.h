/********************************************************************
* 文件名称：http_reassemble.h
* 创建人员：tianhuimeng
* 创建日期：2016-12-08
* 文件描述：http报文重组,对body体支持content-len及chunked格式
*********************************************************************/
#ifndef __HTTP_REASSEMBLE_H__
#define __HTTP_REASSEMBLE_H__

#include "run_base.h"
#include "http_def.h"

typedef enum{
	STATUS_INIT,
	STATUS_HEAD_START,
	STATUS_HEAD_DONE,
	STATUS_BODY_START,
	STATUS_BODY_DONE,
}HTTP_REASSEMBLE_STATUS;


typedef struct http_chunked_ctx_s {
    uint32_t         state;
    uint32_t         size;
    uint32_t         length;
	uint32_t         content_len; //当前获取的总长度
	//uint32_t         chunk_num;
	uint32_t         frag_num;
}http_chunked_ctx_t;

typedef struct{	
	run_buf_t *final_body;
	//runtime change
	run_buf_t *chunk_frag_buf;
	//run_buf_t *chunk_offset_buf;
	run_chain_link_t *chunk_offset_chain_link;
}chunk_assemble_param_t;

//http重组类
class CHTTPReassemble  
{
public:
	
	CHTTPReassemble(int is_response){
		init(is_response);
	}

	~CHTTPReassemble(){
		close();
	}
	
	void reset()
	{
		if (get_status() == STATUS_INIT){
			return;
		}
		
		close();
		init(m_bresponse);
	}

	int is_done();
	int is_idle();
	int force_reassemble();
	int add_pkt_process(unsigned char*pkt, uint32_t pkt_len, uint32_t seq_num);
	
	run_str_t get_body(){
		return m_body;
	}
	run_str_t get_head(){
		return m_head;
	}

	int is_body_complete(){
		return (m_bmiss == 0);
	}
	
	/*for test*/
	int get_status(){
		return m_status;
	}
	void notify_process_later();
private:
	void init(uint32_t is_response);
	void close();
	int check_process(unsigned char *buf, uint32_t len, uint32_t seq_num);
	int head_tmp_buf_add(unsigned char *buf, uint32_t buf_len);
	int head_tmp_buf_assemble(unsigned char *last_buf, uint32_t last_buf_len);
	int head_start_process(unsigned char *buf, uint32_t buf_len, run_str_t & rest);
	int pre_process_header();
	int undo_head_original();
	int undo_body_original();
	int body_frag_process(unsigned char *buf, uint32_t buf_len);
	int process_after_check(unsigned char *buf, uint32_t buf_len);
	
	void save_head_buf_tail(unsigned char *buf, int buf_len);
	unsigned char *get_head_with_last_buf_tail(unsigned char *buf, unsigned char *buf_end, unsigned char *first_br_pos);
	unsigned char *get_head_end(unsigned char *buf, int buf_len);
	
	int force_reassemble_body();
	
	int chunk_frag_process(unsigned char *buf, uint32_t buf_len);
	int http_parse_chunked(run_buf_t *b, http_chunked_ctx_t *ctx);
	int http_chunk_offset_cb(run_chain_buf_t *chain_buf, void *priv);
	int chunk_assemble();
	int save_chunk_frag_cpy(unsigned char *buf, uint32_t buf_len);
	int save_chunk_frag_org(unsigned char *buf, uint32_t buf_len);
	int save_chunk_offset(run_buf_t *org_buf, unsigned char *chunk_start, unsigned char *chunk_end);
	
	run_str_t m_head;
	run_str_t m_body;
	
	uint32_t m_content_len;
	uint32_t m_exp_seq;
	int m_status;
	int m_pkt_type;
	
	run_buf_t m_content_buf;
	
	/*head cache buffer*/
	run_chain_link_t m_head_buf_chain;
	unsigned char m_head_last_buf_tail[HTTP_HEAD_END_FLAG_LEN];
	uint32_t m_head_last_buf_tail_len;
	
	uint32_t m_bresponse:1;
	uint32_t m_bchunked:1;
	uint32_t m_bmiss:1;
	uint32_t m_boriginal_head:1;
	uint32_t m_boriginal_body:1;
	uint32_t m_binter_error:1;
	uint32_t m_bpkt_error:1;
	
	//chunk
	http_chunked_ctx_t m_chunk_ctx;
	run_chain_link_t m_chunk_offset_chain_link;
	run_chain_link_t m_chunk_frag_chain_link;
	//pkt_pool
	
	void set_chunked(){
		m_bchunked = 1;
	}
	
	void set_content_len(uint32_t content_len){
		m_content_len = content_len;
	}
	
	void set_body_original(){
		m_boriginal_body = 1;
	}
	
	void set_head_original(){
		m_boriginal_head = 1;
	}
	
	void set_status(int status){
		m_status = status;
	}
	
	void save_exp_seq(uint32_t exp_seq){
		m_exp_seq = exp_seq;
	}

	int check_seq_ok(uint32_t seq_num){
		return (m_exp_seq == seq_num);
	}

	int is_valid_pkt_type(int pkt_type){
		/*需要根据方向校验起始片是否正确，对于请求方向，则需要判断是否为POST/GET/PUT,对于响应方向，则判断是否为 HTTP*/
		if (m_bresponse){
			return (pkt_type == HTTP_PKT_TYPE_RESPONSE);
		}
		else {
			return (pkt_type == HTTP_PKT_TYPE_REQ_POST
					|| pkt_type == HTTP_PKT_TYPE_REQ_GET
					|| pkt_type == HTTP_PKT_TYPE_REQ_PUT);
		}
	}
	
	int is_head_tmp_buf_empty()
	{
		return run_chain_link_is_empty(&m_head_buf_chain);
	}
	
	int is_cur_content_body_complete(uint32_t buf_len){
		return (buf_len >= m_content_len || buf_len >= g_max_body_buf_len);
	}
	
	void set_incomplete(){
		m_bmiss = 1;
	}
	
	void set_inter_error(){
		m_binter_error = 1;
		reset();
		set_status(STATUS_BODY_DONE);
		printf("inter error! reset and set done.\n");
		//assert(0);
	}
	
	void set_pkt_error(){
		m_bpkt_error = 1;
		reset();
		set_status(STATUS_BODY_DONE);
		printf("pkt error! reset and set done.\n");
		//assert(0);
	}
	
};

#endif // __HTTP_REASSEMBLE_H__
