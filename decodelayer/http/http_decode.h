/********************************************************************
* 文件名称：http_decode.h
* 创建人员：tianhuimeng
* 创建日期：2016-12-15
* 文件描述：http基础解码，管理请求、响应报文重组、解析等
*********************************************************************/
#ifndef __HTTP_DECODE_H__
#define __HTTP_DECODE_H__

#include "run_base.h"
#include "http_reassemble.h"
#include "http_pkt_parse.h"

typedef enum
{
	HTTP_BODY_ZIP_NO = 0,
	HTTP_BODY_ZIP_GZIP,
	HTTP_BODY_ZIP_DEFLATE,
}HTTP_BODY_ZIP_TYPE_E;

typedef struct{
	int is_complete;
	int is_newalloc;
	int zip_type;
	run_str_t body;
}http_body_t;

class CHTTPDecode  
{
public:

	CHTTPDecode(uint32_t has_ack = 0):m_request_reassemble(0),m_response_reassemble(1)
	{
		init(has_ack);
	}
	~CHTTPDecode(){
		close();
	}
	void reset(){
		m_request_reassemble.reset();
		m_response_reassemble.reset();

		m_request_parse.reset();
		m_response_parse.reset();
		
		close();
		init(m_bhas_ack);
	}
	
	void set_ack(uint32_t has_ack){
		m_bhas_ack = has_ack;
	}
	
	int add_pkt(unsigned char *pkt, uint32_t pkt_len, int direction, uint32_t seq);
	int is_done();	
	int is_mine(unsigned char *pkt, uint32_t pkt_len, int direction);
	int force_reassemble();


	/*获取数据API*/
	run_str_t get_uri(){
		return m_request_parse.get_uri();
	}

	run_str_t  get_url(){
		return m_url;
	}

	int get_method(){
		return m_request_parse.get_method();
	}

	int is_post(){
		return ((m_request_parse.get_method() == HTTP_PKT_TYPE_REQ_POST)
				|| (m_request_parse.get_method() == HTTP_PKT_TYPE_REQ_PUT));
	}

	int get_status(){
		return m_response_parse.get_status();
	}

	run_str_t get_host(){
		return m_request_parse.get_head_field_for_enum(HTTP_HEADER_HOST);
	}

	/*获取整个头部*/
	run_str_t get_req_whole_head(){
		return m_request_parse.get_head();
	}

	run_str_t get_req_header(char *name){
		http_head_elt_t *tmp = m_request_parse.get_head_field_for_name(name);
		if(NULL != tmp){
			return tmp->value;
		}
		else{ 
			run_str_t tmp_str;
			run_str_null(&tmp_str);
			return tmp_str;
		}
	}

	run_str_t get_req_header(HTTP_HEADER_E type){
		return m_request_parse.get_head_field_for_enum(type);
	}
	
	run_str_t get_req_body(){
		return (m_req_body.body);	
	}

	int get_req_body_zip_type(){
		return m_req_body.zip_type;
	}

	int get_req_body_is_complete(){
		return m_req_body.is_complete;
	}


	/*获取整个头部*/
	run_str_t get_rsp_whole_head(){
		return m_response_parse.get_head();
	}
	
	run_str_t get_rsp_header(char *name){
		http_head_elt_t *tmp = m_response_parse.get_head_field_for_name(name);
		if(NULL != tmp){
			return tmp->value;
		}
		else{ 
			run_str_t tmp_str;
			run_str_null(&tmp_str);
			return tmp_str;
		}
	}

	run_str_t get_rsp_header(HTTP_HEADER_E type){
		return m_response_parse.get_head_field_for_enum(type);
	}

	run_str_t get_rsp_body(){
		return (m_rsp_body.body);
	}

	int get_rsp_body_is_complete(){
		return m_rsp_body.is_complete;
	}
	int get_rsp_body_zip_type(){
		return m_rsp_body.zip_type;
	}
	
	CHttpRequestPacket* get_req_paket(){
		return &m_request_parse;
	}

	CHttpResponsePacket* get_rsp_packet(){
		return &m_response_parse;
	}

private:
	void init(uint32_t has_ack=0);
	void close();

	int http_preprocess_headers();
	int http_unzip(run_str_t &body, int zip_type);
	int http_get_zip_type(run_str_t &content_enc);

	int http_preprocess_body();
	void process_url();

	
private:
	
	uint32_t m_bhas_ack:1;
	
	CHTTPReassemble m_request_reassemble;
	CHTTPReassemble m_response_reassemble;

	CHttpRequestPacket	m_request_parse;
	CHttpResponsePacket	m_response_parse;

	//pre process
	http_body_t m_req_body;
	http_body_t m_rsp_body;

	run_str_t m_url;

};


#endif // __HTTP_DECODE_H__
