
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <arpa/inet.h>

#include "decode_layer_struct.h"
#include "tcpsort.h"
#include "decode_protocol.h"

typedef struct tcp_header_s{
	uint16_t src_port;
	uint16_t dst_port;
	uint32_t seq_num;
	uint32_t ack_num;
	uint8_t  :4;
	uint8_t head_len:4;
	uint8_t  flags;
	uint16_t wsv;
	uint16_t checksum;
	uint16_t pading;
}tcp_header_t;

enum decode_protocol_t decode_http( struct packet_info ** pinfo);


CTCPSORT g_tcp_sort;

enum decode_protocol_t decode_tcp(struct packet_info ** pinfo)
{
	uint8_t *buf = (*pinfo)->curinfo.buf;
	int len = (*pinfo)->curinfo.len;

	if((unsigned int)len <= sizeof(tcp_header_t) )
		return DECODE_PROTOCOL_NONE;
		
	tcp_header_t * ptcp = (tcp_header_t*)(buf);
	(*pinfo)->tcpinfo.sport = ntohs(ptcp->src_port);
	(*pinfo)->tcpinfo.dport = ntohs(ptcp->dst_port);

	(*pinfo)->tcpinfo.seq = ptcp->seq_num;
	(*pinfo)->tcpinfo.ack = ptcp->ack_num;
	(*pinfo)->tcpinfo.flags = ptcp->flags;

	int tcpheadlen = ptcp->head_len*4;
	
	struct TCPSORT_PACKET_NODE* pretlist;
	struct TCPSORT_PACKET_NODE* prettmp;

	memmove(buf, &buf[tcpheadlen], len-tcpheadlen);
	(*pinfo)->curinfo.len -= tcpheadlen;
	pretlist = g_tcp_sort.GetSortPacketList(pinfo);

	while(pretlist != NULL)
	{
		decode_http(&pretlist->pinfo);
		prettmp = pretlist;
		pretlist = pretlist->next;
		
	}
	
	g_tcp_sort.FreePacketList(pretlist);

	return DECODE_PROTOCOL_NONE;
}

