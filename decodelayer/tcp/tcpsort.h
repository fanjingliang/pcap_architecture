
#pragma once

#include <stdint.h>
#include "run_unix_list.h"

//配置文件读取值的缺省值
#define TCPSORT_MAX_PACKET_COUNT_DEFAULT 4
#define TCPSORT_STAT_TIMEOUT_DEFAULT 600

//插入函数的返回值标记
#define TCPSORT_INSERTPACKET_NO		1
#define TCPSORT_INSERTPACKET_DONE	2
#define TCPSORT_INSERTPACKET_DROP	3

#define TCPSORT_FIN	0x1
#define TCPSORT_SYN	0x2
#define TCPSORT_ACK	0x10

#define TCPSORT_HASHTABLE_COUNT		0x0001ffff//哈希的桶数


//用于进行统计的结构
struct TCPSORT_STATUS
{
	uint32_t		linkcount;	//当前连接的数量
	uint32_t		bufnum;		//当前所有连接中缓存包的个数

	
	uint32_t		packetcount;	//处理过的总包数
	uint32_t		luanxupacketcount;	//乱序包的总数
	uint32_t		errpacketcount;	//重复或错误包的总数

	uint32_t		jiaocha_havenew;	//交叉，但包含了一部分新数据
	uint32_t		jiaocha_nonew;		//交叉，但没有包含新数据，只是重复包
	uint32_t		changed;			//某方向弹出数据，但反方向还有缓存数据
};

struct TCPLINK_STATUS
{
	uint32_t		linkcount;			//连接的总数量
	uint32_t		lostcount[2];		//丢数据的连接的总数量, 区分上下行
	uint64_t		packetbytes[2];	//处理的所有包的总字节数, 分别表示上行和下行, 该值不包含丢失的字节数
	uint64_t		lostbytes[2];		//丢失的总字节数
};

struct TCPSORT_PACKET_NODE
{
	list_node_t  		list;				//用于进行链表操作
	struct TCPSORT_PACKET_NODE	*next;		//用于返回给上级处理的单链表指针
	struct packet_info *pinfo;			//包的上下文的指针

	//seq ack 以及其增量信息，根据与连接的方向的一致性都可以确定
	uint32_t				seqadd; 			//本次包导致的增量
	uint32_t				seqforce1;			// 1表示是强制性的增加1，主要是握手时
	uint64_t				seq;
	uint64_t				ack;
};

struct TCPSORT_CONNECT_NODE
{
	struct IPv4ConnectAddr		connect;			//连接的4元组信息
	list_node_t			list_h;				//用于哈希表的操作
	uint32_t				same_direction;		//这个是临时变量，表示当前处理的这个包与连接的方向是否一致
	uint64_t				seq1;				//期待的方向1的包的SEQ	
	uint64_t				seq2;				//期待的方向2的包的SEQ	
	list_node_t			list_p1;			//用于缓存数据包的链表的操作, 方向1, 与连接方向一致的
	list_node_t			list_p2; 			//用于缓存数据包的链表的操作, 方向2，与连接方向不一致的
	uint16_t				packetcount;		//当前连接缓存的数据包的数量
	int 				lost_data_flag[2]; 	//该连接是否丢失字节
};

class CTCPSORT
{
public:
	CTCPSORT();
	~CTCPSORT();

protected:
	uint32_t HASHKEY(struct IPv4ConnectAddr* conn);
	uint32_t SEQ(packet_info * context);
	uint32_t ACK(packet_info * context);
	uint8_t  FLAGS(packet_info * context);
	uint64_t GetSeq64(struct TCPSORT_CONNECT_NODE* node,uint64_t seq);	
	uint64_t GetAck64(struct TCPSORT_CONNECT_NODE* node,uint64_t ack);

	/*****************************************************************
	基础包操作函数
	这部分的函数用于包，节点的建立
	*****************************************************************/
	struct TCPSORT_PACKET_NODE*		new_packet_node(struct TCPSORT_CONNECT_NODE* node,
								packet_info * context);
	struct TCPSORT_PACKET_NODE*		new_packet_node_real(struct TCPSORT_PACKET_NODE* ppacket);
	struct TCPSORT_CONNECT_NODE* 	new_connect_node(packet_info * context);
	void free_packet_node(struct TCPSORT_PACKET_NODE* ppacket);
	void free_connect_node(struct TCPSORT_CONNECT_NODE* pconn);
	struct TCPSORT_PACKET_NODE*  packet_cut(struct TCPSORT_PACKET_NODE* node, uint64_t currentseq, uint64_t nextseq);	

	/*****************************************************************
	链表操作部分
	这部分的函数主要用于操作链表。包括添加到链表，从链表超时，
	以及执行超时操作等
	*****************************************************************/
	void packetlist_insert(struct TCPSORT_PACKET_NODE* pos, struct TCPSORT_PACKET_NODE* node);
	void packetlist_del(struct TCPSORT_PACKET_NODE* node);
	void return_packetlist_add(struct TCPSORT_PACKET_NODE* *outhead, struct TCPSORT_PACKET_NODE* *outtmp, struct TCPSORT_PACKET_NODE* node);
	void connlist_add(list_node_t* head,struct TCPSORT_CONNECT_NODE *node);
	void connlist_del(struct TCPSORT_CONNECT_NODE* node);

	/*****************************************************************
	哈希表操作部分
	这是TCP排序的主要部分，实现了
	包向连接节点的插入，从连接节点返回可用包，从节点删除已处理包，
	删除连接节点等一系列操作。
	*****************************************************************/
	struct TCPSORT_CONNECT_NODE*  hashtable_search_connect(
					list_node_t * connecthead,
					struct IPv4ConnectAddr* connectinfo);
	uint32_t hashtable_insert_packet_list(
					struct TCPSORT_CONNECT_NODE* pconnect,
					struct TCPSORT_PACKET_NODE* packet,
					uint64_t *listseq, 
					list_node_t * listhead);
	uint32_t hashtable_insert_packet(
					struct TCPSORT_CONNECT_NODE* pconnect, 
					struct TCPSORT_PACKET_NODE* packet);
	struct TCPSORT_PACKET_NODE*  hashtable_return_packet_list(
					struct TCPSORT_CONNECT_NODE* pconnect,
					struct TCPSORT_PACKET_NODE* pnewpacket,
					uint64_t *listseq, 
					list_node_t * listhead);
	struct TCPSORT_PACKET_NODE*  hashtable_return_packet(
					struct TCPSORT_CONNECT_NODE* pconnect,
					struct TCPSORT_PACKET_NODE* pnewpacket);

public:
	/*****************************************************************
	用于在需要对tcp层进行排序的地方使用的接口
	*****************************************************************/
	struct TCPSORT_PACKET_NODE* GetSortPacketList(packet_info **context);
	struct TCPSORT_PACKET_NODE* GetPacketList(struct IPv4ConnectAddr *ConnAddr);
	void FreePacketList(struct TCPSORT_PACKET_NODE* listhead);

	/*****************************************************************
	给调试输出留的接口
	*****************************************************************/
	void display_return(struct TCPSORT_PACKET_NODE* ppacket);
	void display_packet(struct TCPSORT_PACKET_NODE* ppacket);
	void display_connect_packets(struct TCPSORT_CONNECT_NODE* pconn);

	/*****************************************************************
	给统计部分留的接口
	*****************************************************************/
	void tongji_get_status(struct TCPSORT_STATUS * stat) ;
	int tongji_get_link_status(struct TCPLINK_STATUS * linkstat);
	
	/*****************************************************************
	配置值的设置,没有进行设置的自动使用默认值。
	*****************************************************************/
	void cfg_set_maxpacketcount(uint32_t count) { m_cfg_max_packet_count = count; }
	void cfg_set_stattimeout(uint32_t sec) { m_cfg_stat_timeout = sec; }
	uint32_t cfg_get_stattimeout() {return m_cfg_stat_timeout; }
	
protected:
	
	list_node_t	m_hashtable[TCPSORT_HASHTABLE_COUNT+1];
	//统计部分的变量
	struct	TCPSORT_STATUS m_status;
	struct TCPLINK_STATUS m_link_status;
	uint32_t m_status_stamp;		//状态用的时间戳，标记连接状态的清空时间
	//通过配置文件读入的值
	uint32_t		m_cfg_max_packet_count;	//连接最大缓存的包的数量
	uint32_t		m_cfg_stat_timeout;	//状态重置的时间间隔
};




