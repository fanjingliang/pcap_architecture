
#include <stdint.h>
#include "mid.h"
#include "run_alloc.h"
#include <arpa/inet.h>
#include "tools/file.h"
#include "decode_layer_struct.h"
#include "tcpsort.h"


CTCPSORT::CTCPSORT()
{

	for(uint32_t i = 0 ; i <= TCPSORT_HASHTABLE_COUNT ; i++)
	{
		INIT_LIST_HEAD(&m_hashtable[i]);
	}

	m_status_stamp = 0;
	memset(&m_status,0,sizeof(struct TCPSORT_STATUS));
	memset(&m_link_status,0,sizeof(struct TCPSORT_STATUS));
	//设置配置信息的默认值
	m_cfg_max_packet_count = TCPSORT_MAX_PACKET_COUNT_DEFAULT;
	m_cfg_stat_timeout = TCPSORT_STAT_TIMEOUT_DEFAULT;
}

CTCPSORT::~CTCPSORT()
{
	//释放所有的连接的包和节点
	struct TCPSORT_CONNECT_NODE *pnode;
	list_node_t * listnode;
	list_node_t * listhead;
	list_node_t * listtmp;
	
	for(uint32_t i = 0 ; i <= TCPSORT_HASHTABLE_COUNT ; i++)
	{
		listhead = &m_hashtable[i];
		listnode = listhead->next;

		while(listnode != listhead)
		{
			listtmp = listnode->next;
			pnode = LIST_ENTRY(listnode,struct TCPSORT_CONNECT_NODE,list_h);
			free_connect_node(pnode);
			listnode = listtmp;
		}
	}
}

/*
对包进行排序，并返回当前连接中已经可用的包的链表
调用方可以通过遍历这个链表来依次处理各个包

返回值:
成功	返回一个可处理的包链表，调用者依次处理这些包即可。
			返回链表中结构的next可用，prev无意义。
			遍历时next为NULL表示结束
失败	没有内存了，无法进行排序处理，调用者直接处理包即可
*/
struct TCPSORT_PACKET_NODE* CTCPSORT::GetSortPacketList(packet_info **context)
{
	struct TCPSORT_PACKET_NODE * ppacket = NULL;
	struct TCPSORT_CONNECT_NODE *pconnect = NULL;

	
	IPv4ConnectAddr connaddr;
	connaddr.src = (*context)->ipinfo.src;
	connaddr.dst = (*context)->ipinfo.dst;
	connaddr.sport = (*context)->tcpinfo.sport;
	connaddr.dport = (*context)->tcpinfo.dport;
	
	uint32_t hashkey = HASHKEY(&connaddr);
	uint32_t ret = TCPSORT_INSERTPACKET_DROP;
	//查找当前包所属的连接
	pconnect = hashtable_search_connect(&m_hashtable[hashkey],&connaddr);

	if(pconnect == NULL) 
	{
		pconnect = new_connect_node(*context);

		if(pconnect == NULL)
		{
			goto err;
		}
		
		connlist_add(&m_hashtable[hashkey],pconnect);
	}

	//建立一个封装包，里面的内容先使用参数传来的指针，如非必要不进行内容的复制。
	ppacket = new_packet_node(pconnect,*context);

	if(ppacket == NULL)
	{
		goto err;
	}

	*context = NULL;

	//统计信息更新
	m_status.packetcount ++;
	ret = hashtable_insert_packet(pconnect,ppacket);

	if(ret == TCPSORT_INSERTPACKET_NO)
	{
		return ppacket;
	}
	else if( ret == TCPSORT_INSERTPACKET_DONE)
	{
		struct TCPSORT_PACKET_NODE * retlist;
		retlist = hashtable_return_packet(pconnect,ppacket);
		return retlist;
	}	
	else //if( ret == TCPSORT_INSERTPACKET_DROP)
	{
		//包没有被返回处理，直接在这里删除
		free_packet_node(ppacket);		
		//统计信息更新
		m_status.errpacketcount ++;
		return NULL;
	}

err:
	return NULL;
}

/*===========================================================================
*函数名称：GetPacketList
*参数信息：ConnAddr为通讯双方ip及端口号
*函数返回：成功则返回一个可处理的包链表；否则返回NULL
*函数功能：通过ConnAddr 查找ConnAddr对应的链接中缓存的包
*函数说明：调用者可依次处理返的包链表中的数据包，链表中结构的next可用，prev无意义
*创建日期：2016-12-15
*创建人员：郭鹏远
===========================================================================*/		
struct TCPSORT_PACKET_NODE* CTCPSORT::GetPacketList(IPv4ConnectAddr *ConnAddr)
{
	struct TCPSORT_CONNECT_NODE * pconnect = NULL;
	struct TCPSORT_PACKET_NODE * preturn = NULL;
	struct TCPSORT_PACKET_NODE * ptmp = NULL;

	//用于缓存数据包的链表的操作，与链接节的方向一致
	struct TCPSORT_PACKET_NODE * ppacket1;
	list_node_t * listpnode1; 
	list_node_t * listphead1;
	list_node_t * listptmp1;

	//用于缓存数据包的链表的操作，与链接节的方向不一致
	struct TCPSORT_PACKET_NODE * ppacket2;
	list_node_t * listpnode2; 
	list_node_t * listphead2;
	list_node_t * listptmp2;
		
	uint32_t hashkey = HASHKEY(ConnAddr);
	//查找当前包所属的连接	
	pconnect = hashtable_search_connect(&m_hashtable[hashkey], ConnAddr);	

	if(pconnect == NULL)
	{
		return NULL;
	}
 
	listphead1 = &pconnect->list_p1;
	listpnode1 = listphead1->next;
	listphead2 = &pconnect->list_p2;
	listpnode2 = listphead2->next;

	//遍历缓存数据包的链表，与链接的方向不一致
	//从原来的缓存链表中摘除，加入到返回列表中
	while(listpnode1 != listphead1)
	{
		listptmp1 = listpnode1->next;
		ppacket1 = LIST_ENTRY(listpnode1,struct TCPSORT_PACKET_NODE,list);

		//遍历缓存数据包的反向链表
		while(listpnode2 != listphead2)
		{
			listptmp2 = listpnode2->next;
			ppacket2 = LIST_ENTRY(listpnode2,struct TCPSORT_PACKET_NODE,list);

			//根据ack的值，返回反向链表中seq比ack小的包
			if(ppacket2->seq + ppacket2->seqadd <=  ppacket1->ack)
			{
				packetlist_del(ppacket2);
				return_packetlist_add(&preturn,&ptmp,ppacket2);
			}
			else
			{
				break;
			}

			listpnode2 = listptmp2;	
		}

		packetlist_del(ppacket1);
		return_packetlist_add(&preturn,&ptmp,ppacket1);
		listpnode1 = listptmp1;
	}

	//遍历缓存数据包的反向链表
	while(listpnode2 != listphead2)
	{
		listptmp2 = listpnode2->next;
		ppacket2 = LIST_ENTRY(listpnode2,struct TCPSORT_PACKET_NODE,list);
		//从原来的缓存链表中摘除，加入到返回列表中
		packetlist_del(ppacket2);
		return_packetlist_add(&preturn,&ptmp,ppacket2);
		listpnode2 = listptmp2;
	}
	
    free_connect_node(pconnect);
	return preturn;

}


/*
释放已经处理完的包链表
*/
void CTCPSORT::FreePacketList(struct TCPSORT_PACKET_NODE* listhead)
{
	struct TCPSORT_PACKET_NODE * ppacket = NULL;
	struct TCPSORT_PACKET_NODE * ptmp = NULL;
	ppacket = listhead;

	while(ppacket != NULL)
	{
		ptmp = ppacket->next;
		free_packet_node(ppacket);
		ppacket = ptmp;
	}
}


/*
 * 根据当前连接中的期待seq值，来判断当前参数中的seq值，实际应该对应的64位的seq值。
 * 由于存在待查找值比期待值小的情况，所以需要当前值至少比0xffffffff大才行。
 * 这就要求连接的第一个包的seq值需要加上这个0xfffffffff
 */
uint64_t CTCPSORT::GetSeq64(struct TCPSORT_CONNECT_NODE* node,uint64_t seq)
{
	uint64_t * pcurrentseq;

	if(node->same_direction == 1)
	{
		pcurrentseq = &node->seq1;
	}
	else
	{
		pcurrentseq = &node->seq2;
	}

	if(*pcurrentseq == 0)
	{
		return seq + 0x100000000ULL;
	}
	
	uint64_t tmpseq;
	int circle = 0;
	tmpseq = *pcurrentseq;

	while( tmpseq > 0x100000000ULL)
	{
		circle ++;
		tmpseq -= 0x100000000ULL;
	}

	if(seq < tmpseq && (tmpseq - seq) > 0x1000000)
	{
		return seq + (circle+1) * 0x100000000ULL;
	}
	else if( tmpseq < seq && ( seq - tmpseq) > 0x1000000)
	{
		return seq + (circle-1) * 0x100000000ULL;
	}
	else
	{
		return seq + circle * 0x100000000ULL;
	}
	
}

uint64_t CTCPSORT::GetAck64(struct TCPSORT_CONNECT_NODE* node,uint64_t ack)
{
	uint32_t direction_bak = node->same_direction;
	
	if(ack == 0) return 0;
	
	if(node->same_direction ==1)
	{
		node->same_direction = 0;
	}
	else
	{
		node->same_direction = 1;
	}

	uint64_t ack64 = GetSeq64(node,ack);
	node->same_direction = direction_bak;
	return ack64;
}


/*
构造一个包装包，用于后面的处理
没有进行包内容的复制，直接使用传过来的指针
*/
struct TCPSORT_PACKET_NODE*	CTCPSORT::new_packet_node(struct TCPSORT_CONNECT_NODE* node,
					packet_info * context)
{
	(void)node;
	struct TCPSORT_PACKET_NODE*	ppacket = NULL;
	ppacket = (struct TCPSORT_PACKET_NODE*)run_malloc(MID_CTCPSORT_NEW_PACKET_NODE , sizeof(struct TCPSORT_PACKET_NODE));

	if(ppacket == NULL)
	{
		return NULL;
	}

	memset(	ppacket, 0, sizeof(struct TCPSORT_PACKET_NODE));
	ppacket->pinfo = context;
	ppacket->seq = GetSeq64(node,SEQ(context));
	ppacket->seqadd = context->curinfo.len;
	ppacket->ack = GetAck64(node,ACK(context));
	uint32_t flag;
	flag = FLAGS(context);

	//握手的过程的包都是seq+1
	//握手的一个包syn 无法被使用所以不需要考虑他
	if( (flag == (TCPSORT_SYN+TCPSORT_ACK) ) ||
		(flag & TCPSORT_FIN) || flag == TCPSORT_SYN )
	{
		ppacket->seqadd = 1;
		ppacket->seqforce1 = 1;
	}

	//其他几个变量已经被memset置空了，不需要再赋值了。
	return ppacket;
}

/*
 * 对一个包进行切割，如果不需要切割（即正好处于前后2个seq之间），那么直接返回
 * 
 *
 * node 为原始的包的信息部分，包括tcpudpcontext
 * currentseq 为当期望的seq值。如果这个包要插入链表，那么这个值是他前一个包的值+前一个包的长度
 * nextseq 如果这个包需要插入到链表中，那么这个值为非0的，表示它后一个包的seq值。如果不要插链表这个值是0
 */
struct TCPSORT_PACKET_NODE* CTCPSORT::packet_cut(struct TCPSORT_PACKET_NODE* node, uint64_t currentseq, uint64_t nextseq)
{
	uint64_t beginpos = 0;
	int64_t length = node->seqadd;
	uint16_t * pip_len;
	char * contentptr;

	if(nextseq == 0)
	{
		if(node->seq >= currentseq)
		{
			return node;
		}
	}
	else
	{
		if(node->seq >= currentseq && node->seq+node->seqadd <= nextseq)
		{
			return node;
		}
	}
	
	if(node->seq <= currentseq)
	{
		beginpos = currentseq - node->seq;
		length -= beginpos;
	}

	if( (nextseq != 0) && (node->seq + node->seqadd > nextseq) )
	{
		length -= node->seq + node->seqadd - nextseq;
	}

	if(length < 0)
	{
		return NULL;
	}
	
	node->seq = node->seq + beginpos;
	node->seqadd = length;
	
	if(length >4000)
	{
		//printf("数据包长度大于:4000,分配异常\n");
		goto err;
	}

	node->pinfo->curinfo.len = length;
	
	contentptr = (char *)(node->pinfo->curinfo.buf);
	memmove(node->pinfo->curinfo.buf,(void*)&contentptr[beginpos],length);
	

	return node;
err:
	
	return NULL;
}

/*
根据包的信息构造一个连接节点
需要根据这个包的内容来确定seq的相关信息。
*/
struct TCPSORT_CONNECT_NODE* CTCPSORT::new_connect_node(packet_info * context)
{
	struct TCPSORT_CONNECT_NODE* pconnect = NULL;
	pconnect = (struct TCPSORT_CONNECT_NODE*)run_malloc(MID_CTCPSORT_NEW_CONNECT_NODE , sizeof(struct TCPSORT_CONNECT_NODE));

	if(pconnect == NULL)
	{
		return NULL;
	}
	
	uint32_t flag;
	flag = FLAGS(context);

	//判断握手flag 的意义是可以尽可能的将连接的方向设置为发起方到远端方
	if(flag == (TCPSORT_SYN+TCPSORT_ACK) )
	{
		//表示包的方向是远端方到发起方
		pconnect->connect.src= context->ipinfo.dst;
		pconnect->connect.sport = context->tcpinfo.dport;
		
		
		pconnect->connect.dst = context->ipinfo.src;
		pconnect->connect.dport = context->tcpinfo.sport;
		
		pconnect->same_direction = 0;
	}
	else // if( flag == TCPSORT_SYN ) //其他flag都认为是发起方到远端方的方向
	{
		//表示包的方向是发起方到远端方
		pconnect->connect.src= context->ipinfo.src;
		pconnect->connect.sport = context->tcpinfo.sport;
		
		
		pconnect->connect.dst = context->ipinfo.dst;
		pconnect->connect.dport = context->tcpinfo.dport;
		
		pconnect->same_direction = 1;
	}
	
	pconnect->packetcount = 0;

	//需要初始化当前连接缓存包链表的头部
	INIT_LIST_HEAD(&pconnect->list_p1);
	INIT_LIST_HEAD(&pconnect->list_p2);

	//根据此包的内容初始化 sequence
	pconnect->seq1 = 0;
	pconnect->seq2 = 0;
	pconnect->lost_data_flag[0] = pconnect->lost_data_flag[1] = 0;

	//统计信息更新
	m_status.linkcount ++;
	m_link_status.linkcount ++;
	return pconnect;
	
}

/*
释放一个包节点的内存
*/
void CTCPSORT::free_packet_node(struct TCPSORT_PACKET_NODE* ppacket)
{
	run_free((void*)ppacket->pinfo->curinfo.buf);
	run_free((void*)ppacket->pinfo);
	
	run_free(ppacket);
}

/*
释放一个连接节点，这个过程要释放所有的包结构
*/
void CTCPSORT::free_connect_node(struct TCPSORT_CONNECT_NODE* pconn)
{
	struct TCPSORT_PACKET_NODE* ppacket;
	list_node_t * plisthead = &pconn->list_p1;
	list_node_t * ptmp = NULL;
	list_node_t * plistpacket = plisthead->next;

	//删除连接下面的数据包
	while(plistpacket != plisthead)
	{
		ptmp = plistpacket->next;
		ppacket = LIST_ENTRY(plistpacket,struct TCPSORT_PACKET_NODE,list);
		free_packet_node(ppacket);
		plistpacket = ptmp;
	}
	
	plisthead = &pconn->list_p2;
	plistpacket = plisthead->next;
	//删除连接下面的数据包
	while(plistpacket != plisthead)
	{
		ptmp = plistpacket->next;
		ppacket = LIST_ENTRY(plistpacket,struct TCPSORT_PACKET_NODE,list);
		free_packet_node(ppacket);
		plistpacket = ptmp;
	}
	
	//删除连接所在的哈希表，超时链表
	connlist_del(pconn);	
	run_free(pconn);
	//统计信息更新
	m_status.linkcount --;
}


/*
调试用
*/
void CTCPSORT::display_return(struct TCPSORT_PACKET_NODE* ppacket)
{
	while(ppacket != NULL)
	{
		display_packet(ppacket);
		ppacket = ppacket->next;	
	}
}

/*
调试用
*/
void CTCPSORT::display_packet(struct TCPSORT_PACKET_NODE* ppacket)
{
	printf("=======================\n");
	printf("packet info\n");
	printf("seq    = %llx\n", (unsigned long long)ppacket->seq);
	printf("seqadd = %lld\n", (unsigned long long)ppacket->seqadd);
	printf("ack    = %llx\n", (unsigned long long)ppacket->ack);
	printf("=======================\n");
}

/*
调试用
*/
void CTCPSORT::display_connect_packets(struct TCPSORT_CONNECT_NODE* pconn)
{
	struct TCPSORT_PACKET_NODE* ppacket;
	list_node_t * plisthead = &pconn->list_p1;
	list_node_t * plistpacket = plisthead->next;
	uint32_t index1 = 1;
	uint32_t index2 = 1;
	printf("---------------------------------\n");
	printf("connect %p\n",pconn);
	struct in_addr addr ; 
	addr.s_addr = pconn->connect.src;
	printf("%s - %d- ",inet_ntoa(addr), pconn->connect.sport);
	addr.s_addr = pconn->connect.dst;
	printf("%s - %d\n",	inet_ntoa(addr), pconn->connect.dport);
	printf("connect seq1 = %llx\n", (unsigned long long)pconn->seq1);
	printf("connect seq2 = %llx\n", (unsigned long long)pconn->seq2);

	while(plistpacket != plisthead)
	{
		ppacket = LIST_ENTRY(plistpacket,struct TCPSORT_PACKET_NODE,list);
		printf("packet index1 %d\n",index1++);
		display_packet(ppacket);
		plistpacket = plistpacket->next;
	}
	
	plisthead = &pconn->list_p2;
	plistpacket = plisthead->next;

	while(plistpacket != plisthead)
	{
		ppacket = LIST_ENTRY(plistpacket,struct TCPSORT_PACKET_NODE,list);
		printf("packet index2 %d\n",index2++);
		display_packet(ppacket);
		plistpacket = plistpacket->next;
	}
	
	printf("---------------------------------\n");
}

/*
从哈希表的某个桶上查找指定的连接节点
返回这个节点
*/
struct TCPSORT_CONNECT_NODE*  CTCPSORT::hashtable_search_connect(
							list_node_t * connecthead,
							IPv4ConnectAddr* conninfo)
{
	list_node_t * listnode;
	struct TCPSORT_CONNECT_NODE* pconnect = NULL;
	listnode = connecthead->next;

	while( listnode != connecthead)
	{
		pconnect = LIST_ENTRY(listnode,struct TCPSORT_CONNECT_NODE,list_h);
		
		if(pconnect->connect.src== conninfo->src&&
			pconnect->connect.dst== conninfo->dst&&
			pconnect->connect.sport== conninfo->sport&&
			pconnect->connect.dport== conninfo->dport)
		{
			pconnect->same_direction = 1;
			goto found;
		}

		if(pconnect->connect.src== conninfo->dst&&
			pconnect->connect.dst== conninfo->src&&
			pconnect->connect.sport== conninfo->dport&&
			pconnect->connect.dport== conninfo->sport)
		{
			pconnect->same_direction = 0;
			goto found;
		}
		
		listnode = listnode->next;
	}

	return NULL;
	
found:
	return pconnect;
}

/*
将一个包插入到连接的正确位置
返回值表示 是否插入到了缓存链表中
TCPSORT_INSERTPACKET_NO		没有插入可以直接处理
TCPSORT_INSERTPACKET_DONE	插入了
TCPSORT_INSERTPACKET_DROP	乱序包或错误包，不需要处理，丢弃即可
*/
uint32_t CTCPSORT::hashtable_insert_packet(
							struct TCPSORT_CONNECT_NODE* pconnect, 
							struct TCPSORT_PACKET_NODE* pnewpacket)
{
	//新包是否与连接的方向一致
	if(pconnect->same_direction == 0)
	{
		return hashtable_insert_packet_list(pconnect,pnewpacket,&pconnect->seq2, &pconnect->list_p2);
	}
	
	else
	{
		return hashtable_insert_packet_list(pconnect,pnewpacket,&pconnect->seq1, &pconnect->list_p1);
	}
}

uint32_t CTCPSORT::hashtable_insert_packet_list(
							struct TCPSORT_CONNECT_NODE* pconnect,
							struct TCPSORT_PACKET_NODE* pnewpacket,
							uint64_t *listseq, 
							list_node_t * listhead)
{
	list_node_t * listnode = listhead->next;	
	struct TCPSORT_PACKET_NODE* ppacket = NULL;
	struct TCPSORT_PACKET_NODE* ptmppacket = NULL;
	
	//SEQ为0表示原来没有包，第一个包直接返回
	if( *listseq == 0)
	{
		//第一个包
		*listseq = pnewpacket->seq + pnewpacket->seqadd;
		pnewpacket->next = NULL;

		if(pnewpacket->seqforce1 == 0)
		{
			m_link_status.packetbytes[pconnect->same_direction] += pnewpacket->seqadd;
		}
		
		return TCPSORT_INSERTPACKET_NO;
	}

	/*
	如何确定新来的一个包是否是顺序来的包
	新包必须与期待的SEQ一致才是顺序包，否则需要在链表中缓存
	*/
	//如果当前连接的包链表中是空的，并且当前包是顺序的那么就不需要加入链表，直接返回处理即可
	if(listnode == listhead && 
		(pnewpacket->seq == (*listseq)) )
	{

		*listseq = pnewpacket->seq + pnewpacket->seqadd;
		pnewpacket->next = NULL;
		
		//printf("顺序包，直接返回处理\n");
		if(pnewpacket->seqforce1 == 0)
		{
			m_link_status.packetbytes[pconnect->same_direction] += pnewpacket->seqadd;

		}
		
		return TCPSORT_INSERTPACKET_NO;
	}

	//重复包丢弃
	if( pnewpacket->seq + pnewpacket->seqadd <= (*listseq) )
	{
		m_status.jiaocha_nonew ++;
		return TCPSORT_INSERTPACKET_DROP;
	}
	
	//如果新包中包含新数据那么进行切割后也可以直接返回
	if(listnode == listhead &&
		(pnewpacket->seq <(*listseq)) &&
		(pnewpacket->seq + pnewpacket->seqadd > (*listseq) ) )
	{

		if( packet_cut(pnewpacket,*listseq,0) == NULL)
		{
			return TCPSORT_INSERTPACKET_DROP;
		}

		*listseq = pnewpacket->seq + pnewpacket->seqadd;
		pnewpacket->next = NULL;
		m_status.jiaocha_havenew ++;
		
		if(pnewpacket->seqforce1 == 0)
		{
			m_link_status.packetbytes[pconnect->same_direction] += pnewpacket->seqadd;
		}
		
		return TCPSORT_INSERTPACKET_NO;
	}


	//负载为0 的包也返回处理,所以需要继续进行后面的插入流程	

	//说明
	//如果包链表非空或者是当前包不是顺序的话，都要先插入到链表中进行排序。
	//插入后才能判断到底有几个包是排序好了的。

	//首先要找出需要排在那个包的前面,缺省是排到头结点的前面，向空链表中插入就是这种情况
	//由于链表的正向顺序是由小到大排序好的，只需要找到比他大的第一个包即可。
	while(listnode != listhead)
	{
		ppacket = LIST_ENTRY(listnode,struct TCPSORT_PACKET_NODE,list);
		
		if( (ppacket->seq > pnewpacket->seq) || ((ppacket->seq == pnewpacket->seq) && pnewpacket->seqadd == 0) ) 
		{
			break;
		}
		else if(ppacket->seq == pnewpacket->seq && pnewpacket->seqadd <= ppacket->seqadd)
		{
			//printf("当前包重复或错误，丢弃2\n");
			return TCPSORT_INSERTPACKET_DROP;
		}
		
		listnode = listnode->next;
	}
	
	ppacket = LIST_ENTRY(listnode,struct TCPSORT_PACKET_NODE,list);
	ptmppacket = LIST_ENTRY(listnode->prev,struct TCPSORT_PACKET_NODE,list);
	uint64_t nextseq = ppacket->seq;
	uint64_t currseq = ptmppacket->seq+ptmppacket->seqadd;
	
	if(listnode == listhead)
	{
		//到了末尾了
		nextseq = 0;
	}
	
	if(listnode->prev == listhead)
	{
		currseq = *listseq;
	}

	if( packet_cut(pnewpacket,currseq,nextseq) == NULL)
	{
		return TCPSORT_INSERTPACKET_DROP;
	}

	packetlist_insert(ppacket, pnewpacket);
	pconnect->packetcount ++;
	return TCPSORT_INSERTPACKET_DONE;
}

/*
从连接的缓存包链表中返回现在可以处理的包（即已排好序的顺序的包）
以及根据ack的值确定的已经必须要处理的包
并返回一个包的链表
返回的链表是个单向链表，next为NULL表示结束
*/
struct TCPSORT_PACKET_NODE*  CTCPSORT::hashtable_return_packet(
							struct TCPSORT_CONNECT_NODE* pconnect, 
							struct TCPSORT_PACKET_NODE* pnewpacket)
{
	uint64_t *listseq;
	list_node_t * listhead;
	struct TCPSORT_PACKET_NODE* preturn = NULL;
	struct TCPSORT_PACKET_NODE* ptmp = NULL;
	struct TCPSORT_PACKET_NODE* ppacket = NULL;
	list_node_t *listnode;
	list_node_t *listnodetmp;
	uint32_t tmp_same_direction = 0; 

	//根据ack的值，返回反向链表中seq比ack小的包
	if(pconnect->same_direction == 0)
	{
		listseq = &pconnect->seq1;
		listhead = &pconnect->list_p1;
	}
	else
	{
		listseq = &pconnect->seq2;
		listhead = &pconnect->list_p2;
	}
	
	tmp_same_direction = (pconnect->same_direction+1)%2;
	listnode = listhead->next;
	listnodetmp = NULL;
	
	while(listnode != listhead)
	{
		listnodetmp = listnode->next;
		ppacket = LIST_ENTRY(listnode,struct TCPSORT_PACKET_NODE,list);

		if(ppacket->seq+ppacket->seqadd <= pnewpacket->ack)
		{
			packetlist_del(ppacket);
			pconnect->packetcount -- ;
			return_packetlist_add(&preturn,&ptmp,ppacket);

			//这里的返回是因为受反方向的ack影响导致的强制输出，需要记录丢包统计
			if(ppacket->seqforce1 == 0)
			{
				m_link_status.packetbytes[tmp_same_direction] += ppacket->seqadd;

			}
			
			m_link_status.lostbytes[tmp_same_direction] += ppacket->seq - (*listseq);

			//如果丢包且该连接的丢包情况没有记录的话，就计入流丢包统计中
			if( (ppacket->seq - (*listseq)) > 0 && pconnect->lost_data_flag[tmp_same_direction] == 0)
			{
				m_link_status.lostcount[tmp_same_direction] ++;
				pconnect->lost_data_flag[tmp_same_direction] = 1;
			}

			//更新连接的seq 值
			*listseq = ppacket->seq + ppacket->seqadd;
		}
		else
		{
			break;
		}
		
		listnode = listnodetmp;
	}

	if(pconnect->same_direction == 0)
	{
		listseq = &pconnect->seq2;
		listhead = &pconnect->list_p2;
	}
	else
	{
		listseq = &pconnect->seq1;
		listhead = &pconnect->list_p1;
	}

	tmp_same_direction = pconnect->same_direction;
	
	//要检测当前的这个包链表中，是否有顺序正确的包，并将其取出。
	
	listnode = listhead->next;
	listnodetmp = NULL;

	while(listnode != listhead)
	{
		//凡是涉及到循环中删除的都要使用临时变量
		listnodetmp = listnode->next;
		ppacket = LIST_ENTRY(listnode,struct TCPSORT_PACKET_NODE,list);

		//顺序包或超多包时，都要返回一个处理
		if(ppacket->seq	== (*listseq) || pconnect->packetcount > m_cfg_max_packet_count)
		{
			packetlist_del(ppacket);
			pconnect->packetcount -- ;
			return_packetlist_add(&preturn,&ptmp,ppacket);
	
			if(ppacket->seqforce1 == 0)
			{
				m_link_status.packetbytes[tmp_same_direction] += ppacket->seqadd;
			}

			m_link_status.lostbytes[tmp_same_direction] += ppacket->seq - (*listseq);

			//如果丢包且该连接的丢包情况没有记录的话，就计入流丢包统计中
			if( (ppacket->seq - (*listseq)) > 0 && pconnect->lost_data_flag[tmp_same_direction] == 0)
			{
				m_link_status.lostcount[tmp_same_direction] ++;
				pconnect->lost_data_flag[tmp_same_direction] = 1;
			}
			
			//更新连接的seq 值
			*listseq = ppacket->seq + ppacket->seqadd;

		}
		else
		{
			break;
		}
		
		listnode = listnodetmp;
	}

	return preturn;	
}


uint32_t CTCPSORT::HASHKEY(IPv4ConnectAddr* conn)
{
	uint32_t tmp;
	tmp = (conn->sport^conn->dport)^((conn->src^conn->dst)&TCPSORT_HASHTABLE_COUNT);
	return tmp;
}

uint32_t CTCPSORT::SEQ(packet_info * context)
{
	return ntohl(context->tcpinfo.seq);
}

uint32_t CTCPSORT::ACK(packet_info * context)
{
	return ntohl(context->tcpinfo.ack);
}

uint8_t  CTCPSORT::FLAGS(packet_info * context)
{
	return context->tcpinfo.flags;
}


/*
在pos位置的前面插入一个包
按原理来说是在 pos的前一个 和 pos之间插入一个，看list.h这个就用list_add_tail
*/
void CTCPSORT::packetlist_insert(struct TCPSORT_PACKET_NODE* pos, struct TCPSORT_PACKET_NODE* node)
{
	list_add_tail(&pos->list,&node->list);

	//状态信息更新
	m_status.bufnum ++;
}
void CTCPSORT::packetlist_del(struct TCPSORT_PACKET_NODE* node)
{
	list_del_node(&node->list);

	//状态信息更新
	m_status.bufnum --;

}
void CTCPSORT::return_packetlist_add(struct TCPSORT_PACKET_NODE* *outhead,
							struct TCPSORT_PACKET_NODE* *outtmp,
							struct TCPSORT_PACKET_NODE* node)
{
	node->next = NULL;
	
	if(*outhead == NULL)
	{
		*outhead = node;
	}
	else
	{
		(*outtmp)->next = node;
	}
	*outtmp = node;
}

/*
将一个连接加入到哈希表中
*/
void CTCPSORT::connlist_add(list_node_t * head,struct TCPSORT_CONNECT_NODE *node)
{
	list_add_tail(head,&node->list_h);
}

void CTCPSORT::connlist_del(struct TCPSORT_CONNECT_NODE* node)
{
	list_del_node(&node->list_h);
}

void CTCPSORT::tongji_get_status(struct TCPSORT_STATUS * stat) 
{
	memcpy(stat, & m_status,sizeof(m_status) );
}

int CTCPSORT::tongji_get_link_status(struct TCPLINK_STATUS * linkstat)
{
	memcpy(linkstat, & m_link_status,sizeof(m_link_status) );
	return 1;	
}


