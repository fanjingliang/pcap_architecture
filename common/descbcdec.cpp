/***********************************************************************
 *     
 *  Filename    :  descbcdec.cpp
 *
 *  Author      :  gpengyuan
 *  Create      :  2016-4-24
 *
 *  Description :
 *                            
 *  Copyright (C), 2016, Beijing Run.Co., Ltd.
 *
 *  ChangeLog   :  zly on 2016-5-31, add des_cbc_decode and des_cbc_dec_real
 *
 ***********************************************************************/
#include <string.h>
#include <stdlib.h>
#include <stdint.h>

#include "run_alloc.h"

#include "mid.h"
#include "encode/descbcdec.h"
#include "encode/base64.h"
  
/*=======================================================================================
* 函数名称:	Des_Decode
* 参数信息:	unsigned char*	src			[IN]输入的des加密数据
* 参数信息:	int				srclen		[IN]des加密数据长度
* 参数信息:	const char*		block		[IN]秘钥block值
* 参数信息:	DES_cblock*		ivec		       [IN]初始化向量ivec值
* 参数信息:	unsigned char*	dsttmp		[IN]J解密后未进行进行格式化的字符串
* 参数信息:	unsigned char*	dst			[OUT]解密并格式化后的明文字符串
* 参数信息:	int*				dstlen		[OUT]解密后明文字符串长度
* 函数返回:	返回解密后的字符串指针、字符串长度和true，失败则返回false
* 函数功能:	DES CBC模式解密
* 创建日期:	2016-04-18
* 创建人员:	gpengyuan
=======================================================================================*/
bool Des_Decode(unsigned char *src, int srclen, const char *block,  DES_cblock *ivec,  unsigned char *dsttmp, unsigned char *dst, int *dstlen)
{
    
	if(src == NULL || srclen <= 0 || block == NULL || ivec == NULL || dsttmp == NULL ||dst == NULL || dstlen ==NULL)
	{
		return false;
	}

	DES_key_schedule ks;	//秘钥变量

	/* 设置密码表 */
    DES_set_key_unchecked((const_DES_cblock*)block, &ks);
    
	/* 加密块链式解密 */
    DES_ncbc_encrypt(src, dsttmp, (long)srclen, &ks, ivec, DES_DECRYPT); //src 和dst均为base解码输出的结果，即pcon，des机密后会秔con进行覆盖


	/*只针对明文为字符串的情况下取出尾数为填充数的情况*/ 
	int j = 0;
	int k =	0;
	int tmplen =0;
	for(j=srclen-1; j>=0; j--)
    {
		if(dsttmp[j] > 7)
			break;
		else
			{
				dsttmp[j] = 0;
				k++;
			}
    }	
	tmplen = srclen - k;	//去除尾数为填充数后的字符串长度

	if(tmplen%2 !=0)
	{
		goto Final;
	}

	if( !hex2bin((const unsigned char *)dsttmp, tmplen, dst, dstlen) )
	{
		goto Final;
	}

	return true;

Final:

	return false;
	

}

bool des_cbc_dec_real(unsigned char * src, int src_len, const char * block, DES_cblock * ivec, unsigned char * dst, int & dst_len)
{
    
	if(src == NULL || src_len <= 0 || block == NULL || ivec == NULL || dst == NULL)
	{
		return false;
	}

	//秘钥变量
	DES_key_schedule ks;

	/* 设置密码表 */
    DES_set_key_unchecked((const_DES_cblock *)block, &ks);
    
	/* 加密块链式解密 */
    DES_ncbc_encrypt(src, dst, (long)src_len, &ks, ivec, DES_DECRYPT);


	/*只针对明文为字符串的情况下取出尾数为填充数的情况*/ 
	int j = 0;
	int k =	0;
	
	for(j= src_len - 1;j >= 0;j--)
    {
		if(dst[j] > 8)
			break;
		
		dst[j] = 0;
		k++;

    }

	//去除尾数为填充数后的字符串长度
	dst_len = src_len - k;
	
	return dst_len > 0;

}

/*=======================================================================================
* 函数名称:	des_cbc_decode
* 参数信息:	arg					[IN]需要解密数据的相关信息
				arg->in_type			[IN]des 解密前数据类型: 1(base64);  0(原始串)
				arg->out_type		[IN]des 解密后数据类型: 1(hex);  0(原始串)
				
* 函数返回:	true:				[OUT]解密成功
				false:				[OUT]解密失败
				arg->dst				[OUT]解密后的数据的地址
				*(arg->dst_len)		[OUT]解密后的数据的长度
				
* 函数功能:	DES CBC模式解密
* 创建日期:	2015-05-31
* 创建人员:	zly
=======================================================================================*/

bool des_cbc_decode(const DES_CBC_ARG_S * arg)
{
	if(arg == NULL || 
		arg->src == NULL || 
		arg->dst == NULL || 
		arg->iv == NULL || 
		arg->key == NULL || 
		arg->dst_len == NULL || 
		arg->src_len <= 0)
	{
		return false;
	}

	uint8_t * dst_b64 = NULL;
	uint8_t * src_tmp = NULL;
	
	int dst_b64_len = 0;
	int src_tmp_len = 0;

	bool ret = false;

	switch(arg->in_type)
	{
		case DES_IN_ORI:
		{
			src_tmp = (uint8_t *)arg->src;
			src_tmp_len = arg->src_len;
			break;
		}

		case DES_IN_BASE64:
		{
			dst_b64 = (uint8_t *)run_malloc(MID_DES_CBC_DECODE, arg->src_len + 1);
			
			if(dst_b64 == NULL)
				return false;
			
			CBase64::Decode((uint8_t *)arg->src, (uint32_t)arg->src_len, (uint8_t *)dst_b64, (uint32_t *)&dst_b64_len);
			
			if(dst_b64_len % 8)
			{
				goto Final;
			}
			
			src_tmp = dst_b64;
			src_tmp_len = dst_b64_len;
			break;
		}
	
		default:
		{
			return false;
		}
	}
	
	ret = des_cbc_dec_real(src_tmp, src_tmp_len, arg->key, arg->iv, (uint8_t *)arg->dst, *(arg->dst_len));
	
	if(!ret)
	{
		goto Final;
	}

	switch(arg->out_type)
	{
		case DES_OUT_ORI:
		{
			break;
		}
		
		case DES_OUT_HEX:
		{
			ret = false;
			ret = hex2byte((uint8_t *)arg->dst, *(arg->dst_len));
			break;
		}
			
		default:
		{
			ret = false;
			goto Final;
		}
	}

	*(arg->dst + *(arg->dst_len)) = '\0';

Final:

	if(dst_b64 != NULL)
	{
		run_free(dst_b64);
		dst_b64 = NULL;
	}

	return ret;
}


/*=======================================================================================
* 函数名称:	hex2bin
* 参数信息:	const char*		data		[IN] 十六进制字符串
* 参数信息:	int				size			[IN] 十六进制字符串长度，必须为2的倍数
* 参数信息:	unsigned char*	converdest	[OUT] 转换后明文字符串的长度
* 参数信息:	int*				outlen		[OUT]转换后的二进制字符数组长度
* 函数返回:	二进制字符数组，需要free函数释放空间，失败返回NULL
* 函数功能:	十六进制字符串转换二进制字节数据
* 创建日期:	2016-04-19
* 创建人员:	gpengyuan
=======================================================================================*/
bool hex2bin(const unsigned char *data, int size, unsigned char *converdest, int *outlen)
{

	if(data == NULL || size<=0 || converdest == NULL || outlen == NULL)
	{
		return false;
	}
    int i = 0;
    int len = 0;
    char char1 = '\0';
    char char2 = '\0';
    unsigned char value = 0;
 
    if (size % 2 != 0) {
        return false;
    }
 
    len = size / 2;
	
    while (i < len) 
	{
        char1 = *data;
        if (char1 >= '0' && char1 <= '9') 
		{
            value = (char1 - '0') << 4;
        }
        else if (char1 >= 'a' && char1 <= 'f') 
		{
            value = (char1 - 'a' + 10) << 4;
        }
        else if (char1 >= 'A' && char1 <= 'F')
		{
            value = (char1 - 'A' + 10) << 4;
        }
        else 
		{
            break;
        }
        data++;
 
        char2 = *data;
        if (char2 >= '0' && char2 <= '9')
		{
            value |= char2 - '0';
        }
        else if (char2 >= 'a' && char2 <= 'f') 
		{
            value |= char2 - 'a' + 10;
        }
        else if (char2 >= 'A' && char2 <= 'F') 
		{
            value |= char2 - 'A' + 10;
        }
        else 

		{
            break;
        }
 
        data++;
        *(converdest + i++) = value;
    }
    *(converdest + i) = '\0';
 
    if (outlen != NULL)
    {
        *outlen = i;
    }
 
    return true;
}

int hex2int(char ch)
{
	if(ch >='0' && ch <= '9')
		return ch -'0';
	if(ch >='A' && ch <= 'F')
		return ch + 10 - 'A';
	if(ch >='a' && ch <= 'f')
		return ch + 10 - 'f';
	return 0;
}

bool is_hex(char ch)
{
	return (ch >='0' && ch <= '9') || 
		(ch >='A' && ch <= 'F') || 
		(ch >='a' && ch <= 'f');
}

bool hex2byte(unsigned char * hex, int & len)
{
	if(hex == NULL || len <= 0 || len % 2 != 0)
	{
		return false;
	}
	
	int i = 0;

	for(i = 0;i < len;i++)
	{
		if(!is_hex(hex[i]))
			return false;
	}

	for(i = 0;i < len;i += 2)
	{
		hex[i/2] = (hex2int(hex[i]) << 4) + hex2int(hex[i + 1]);
	}

	len /= 2;
	hex[len] = '\0';
	
	return true;
}



