/***********************************************************************
 *     
 *  Filename    :  descbcdec.h
 *
 *  Author      :  
 *  Create      :  2016-4-26
 *
 *  Description :
 *                            
 *  Copyright (C), 2016, Beijing Run.Co., Ltd.
 *
 *  ChangeLog   :             
 *
 ***********************************************************************/

#ifndef _DESCBCDEC_H
#define _DESCBCDEC_H
#include <openssl/des.h>


#define DES_IN_ORI			0
#define DES_IN_BASE64		1

#define DES_OUT_ORI			0
#define DES_OUT_HEX			1

struct DES_CBC_ARG_S
{
	const char * key;
	DES_cblock * iv;
	char * src;
	char * dst;
	int * dst_len;
	int src_len;
	short int in_type;
	short int out_type;
};


bool Des_Decode(unsigned char *src, int srclen, const char *block,  DES_cblock *ivec,  unsigned char *dsttmp, unsigned char *dst, int *dstlen);

bool hex2bin(const unsigned char *data, int size, unsigned char *converdest, int *outlen);

bool des_cbc_decode(const DES_CBC_ARG_S * arg);

bool des_cbc_dec_real(unsigned char * src, int src_len, const char * block, DES_cblock * ivec, unsigned char * dst, int & dst_len);

bool hex2byte(unsigned char * hex, int & len);

int hex2int(char ch);

bool is_hex(char ch);

#endif

