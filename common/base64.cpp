// Base64.cpp: implementation of the CBase64 class.
//
//////////////////////////////////////////////////////////////////////

#include "encode/base64.h"
//
//#ifdef _DEBUG
//#undef THIS_FILE
//static char THIS_FILE[]=__FILE__;
//#define new DEBUG_NEW
//#endif


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CBase64::CBase64()
{
	InitEncodingTable();
}

CBase64::~CBase64()
{

}

//初始化Base64的编码表
  
void CBase64::InitEncodingTable()
{
	short i;
	for (i=0;i<26;i++)
		m_EncodingTable[i] = 'A' + i;
	for (i=0;i<26;i++)
		m_EncodingTable[26+i] = 'a' + i;
	for (i=0;i<10;i++)
		m_EncodingTable[52+i] = '0' + i;
	m_EncodingTable[62] = '+';
	m_EncodingTable[63] = '/';
}

//对一段输入字符串进行编码

int32_t CBase64::Encode(uint8_t* SourceStr, 
					 uint32_t SourceStrLen,
					 uint8_t* DestStr,
					 uint32_t DestStrLen,
					 short LineLen)
{ 

	uint8_t	InBuf[4],OutBuf[4];//一次编码操作用到的缓冲区
	uint32_t	InLength;//已经处理完的字符串长度
	uint32_t	OutLength;//输出字符串的长度
	int32_t	Remain;//输入字符串中剩余的字符长度
	uint32_t	CurInLength;//当前已经处理过与读入的字符串长度
	short			OutChNum;//一次编码操作中生成的有效输出字符数
    short			LineLength;//每行的字符数
	short			i;
	

	InLength = 0;
	LineLength = 0;
	OutLength = 0;
	while (1)
	{ 
		Remain = SourceStrLen - InLength;	
		if (Remain <= 0)
			break;				
		if (OutLength + 4 > DestStrLen)
			return -1;

		for (i=0; i<3; i++)
		{
			CurInLength = InLength + i;
			if (CurInLength < SourceStrLen)
				InBuf[i] = SourceStr[CurInLength];
			else
				InBuf[i] = 0;
		} 		
		OutBuf[0] = (InBuf[0] & 0xFC) >> 2;    
		OutBuf[1] = ((InBuf[0] & 0x03) << 4) | ((InBuf[1] & 0xF0) >> 4);    
		OutBuf[2] = ((InBuf[1] & 0x0F) << 2) | ((InBuf[2] & 0xC0) >> 6);    	
		OutBuf[3] = InBuf[2] & 0x3F;
	
		OutChNum = 4;	
		switch (Remain)
		{		
			case 1: 
				OutChNum = 2; 			
				break;		
			case 2: 
				OutChNum = 3; 			
				break;
		} 

		for (i=0; i<OutChNum; i++)
			DestStr[OutLength++] = m_EncodingTable[OutBuf[i]];
		for (i=OutChNum; i<4; i++)
			DestStr[OutLength++] = '=';		
		
		InLength += 3;	
		LineLength += 4;
	
		if (LineLen>0)
		{	
			if (LineLength>=LineLen)
			{
				LineLength = 0;
				DestStr[OutLength++] = '\r';
				DestStr[OutLength++] = '\n';
			}
		}
	}
	DestStr[OutLength] = 0x00;
	return OutLength;
} 

bool CBase64::Encode(char* SourceFileName, char* DestFileName, short LineLen)
{
	std::ifstream*		InFile;//输入文件
	std::ofstream*		OutFile;//输出文件
	char	ch;
	unsigned char	InBuf[4],OutBuf[4];//一次编码操作用到的缓冲区
	short			OutChNum;//一次编码操作中生成的有效输出字符数
    short			EqualNum;//'='的数量
	short			LineLength;
	bool			IsEnd;//是否到了文件结尾
	int				i;


	InFile = new std::ifstream(SourceFileName,std::ios::binary);
	OutFile = new std::ofstream(DestFileName,std::ios::binary);



	if (! *InFile || ! *OutFile)
		return false;
	
	EqualNum = 0;
	LineLength = 0;
	IsEnd = false;
	while (true)
	{ 
		for (i=0; i<3; i++)
		{
#ifdef LINUX_TRANSFORMER
	if ( ( ch = InFile->get() ))
#else
	if (InFile->get( ch))
#endif
				InBuf[i] = ch;
			else
			{				
				if (i != 0)
				{
					InBuf[i] = 0;
					EqualNum++;
				}
				else
				{
					IsEnd = true;
					break;
				}
			}
		}

		if (IsEnd)
			break;

		OutBuf[0] = (InBuf[0] & 0xFC) >> 2;    
		OutBuf[1] = ((InBuf[0] & 0x03) << 4) | ((InBuf[1] & 0xF0) >> 4);    
		OutBuf[2] = ((InBuf[1] & 0x0F) << 2) | ((InBuf[2] & 0xC0) >> 6);    	
		OutBuf[3] = InBuf[2] & 0x3F;
	
		OutChNum = 4 - EqualNum;	
		
		for (i=0; i<OutChNum; i++)
			OutFile->put(m_EncodingTable[OutBuf[i]]);

		for (i=0; i<EqualNum; i++)
			OutFile->put('=');
		
		LineLength += 4;
	
		if (LineLen>0)
		{	
			if (LineLength>=LineLen)
			{
				LineLength = 0;			
				OutFile->write("\r\n",2);
			}
		}
	}
	InFile->close();
	OutFile->close();
	return true;
}

#if 0 
bool CBase64::Decode(uint8_t* SourceStr, 
				uint32_t SourceStrLen,		
				uint8_t* DestStr, 
				uint32_t* DestStrLen)
{ 
	unsigned char	ch;
	unsigned char	InBuf[4],OutBuf[4];//一次解码操作用到的缓冲区
	unsigned long	InLength;//已经处理完的字符串长度
	unsigned long	OutLength;//输出字符串的长度
	short			ChInBuf;//在一次解码操作中已经读入的字符数
	bool			IsIgnore;//是否忽略当前字符
	bool			IsEnd;//是否到了输入字符串的末尾
    short			OutChNum;//一次解码操作中解码出的有效输出字符数

	InLength = 0; 	 
	ChInBuf = 0;
	OutLength = 0;
	IsEnd = false;
	
	while (true) 
	{
		if (InLength >= SourceStrLen)
			break;
		ch = SourceStr[InLength++];	
		IsIgnore = false;
	
		if ((ch >= 'A') && (ch <= 'Z'))
			ch = ch - 'A';
		else if ((ch >= 'a') && (ch <= 'z'))
			ch = ch - 'a' + 26;		
		else if ((ch >= '0') && (ch <= '9'))
			ch = ch - '0' + 52;	
		else if (ch == '+')
			ch = 62;		
		else if (ch == '=')
			IsEnd = true;		
		else if (ch == '/')
			ch = 63;	
		else
			IsIgnore = true; 
	
		if (!IsIgnore) 
		{	
			OutChNum = 3;
			 
			if (IsEnd) 
			{			
				if (ChInBuf == 0)
					break;				
				if ((ChInBuf == 1) || (ChInBuf == 2))
					OutChNum = 1;
				else
					OutChNum = 2;			
				ChInBuf = 3;			
			}
			InBuf[ChInBuf++] = ch;
			if (ChInBuf == 4)
			{			
				ChInBuf = 0;

				OutBuf[0] = (InBuf[0] << 2) | ((InBuf[1] & 0x30) >> 4);			
				OutBuf[1] = ((InBuf[1] & 0x0F) << 4) | ((InBuf[2] & 0x3C) >> 2);			
				OutBuf[2] = ((InBuf[2] & 0x03) << 6) | (InBuf[3] & 0x3F);
					
				for (int i=0; i<OutChNum; i++) 
					DestStr[OutLength++] = OutBuf[i];
			}
			if (IsEnd)
				break;
		}
	}
	*DestStrLen = OutLength;
	return true;
}
#else
bool CBase64::Decode(uint8_t* SourceStr, 
			uint32_t SourceStrLen,		
			uint8_t* DestStr, 
			uint32_t* DestStrLen)
{ 
	static unsigned char map[256] = {
	/*00*/		0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
	/*10*/		0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
	/*20*/		0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0x3e,0xFF,0xFF,0xFF,0x3f,
	/*30*/		0x34,0x35,0x36,0x37,0x38,0x39,0x3a,0x3b,0x3c,0x3d,0xFF,0xFF,0xFF,0xf0,0xFF,0xFF,
	/*40*/		0xFF,0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,
	/*50*/		0x0f,0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0xFF,0xFF,0xFF,0xFF,0xFF,
	/*60*/		0xFF,0x1a,0x1b,0x1c,0x1d,0x1e,0x1f,0x20,0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,
	/*70*/		0x29,0x2a,0x2b,0x2c,0x2d,0x2e,0x2f,0x30,0x31,0x32,0x33,0xFF,0xFF,0xFF,0xFF,0xFF,
	/*80*/		0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
	/*90*/		0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
	/*A0*/		0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
	/*B0*/		0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
	/*C0*/		0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
	/*D0*/		0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
	/*E0*/		0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
	/*F0*/		0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF
	};
	
	uint8_t	ch;
	uint8_t	InBuf[4];//一次解码操作用到的缓冲区
	uint32_t	InLength;//已经处理完的字符串长度
	uint32_t	OutLength;//输出字符串的长度
	short			ChInBuf;//在一次解码操作中已经读入的字符数
	bool			IsEnd;//是否到了输入字符串的末尾
    short			OutChNum;//一次解码操作中解码出的有效输出字符数
	
	InLength = 0; 	 
	ChInBuf = 0;
	OutLength = 0;
	IsEnd = false;
	if (0 == *DestStrLen) 
	{
		*DestStrLen = SourceStrLen;
	}
	
	while (InLength < SourceStrLen) 
	{
		ch = map[SourceStr[InLength++]];
		if (0xFF != ch) 
		{	
			if (0xF0 == ch)//当前字符是'=', 表示解码应该结束
			{	
				if (ChInBuf == 0)
					break;				
				if ((ChInBuf == 1) || (ChInBuf == 2))
					OutChNum = 1;
				else
					OutChNum = 2;			
				ChInBuf = 3;
				IsEnd = true;
			}
			else
				OutChNum = 3;
			
			InBuf[ChInBuf++] = ch;
			if (ChInBuf == 4)
			{	
				//daihw add 20040610
				if (OutLength > *DestStrLen - 4)
				{
					break;
				}

				ChInBuf = 0;
				DestStr[OutLength++] = (InBuf[0] << 2) | ((InBuf[1] & 0x30) >> 4);			
				DestStr[OutLength++] = ((InBuf[1] & 0x0F) << 4) | ((InBuf[2] & 0x3C) >> 2);			
				DestStr[OutLength++] = ((InBuf[2] & 0x03) << 6) | (InBuf[3] & 0x3F);
			}
			
			if (IsEnd)
			{
				OutLength -= 3 - OutChNum;
				break;
			}
		}
	}
	*DestStrLen = OutLength;
	DestStr[OutLength] = '\0';
	return true;
}

#endif

bool CBase64::Decode(char* SourceFileName, char* DestFileName)
{
	std::ifstream*		InFile;//输入文件
	std::ofstream*		OutFile;//输出文件
	char			ch;
	uint8_t			InBuf[4],OutBuf[4];//一次解码操作用到的缓冲区
	short			ChInBuf;//在一次解码操作中已经读入的字符数
	bool			IsIgnore;//是否忽略当前字符
	bool			IsEnd;//是否到了输入字符串的末尾
    short			OutChNum;//一次解码操作中解码出的有效输出字符数

	InFile = new std::ifstream(SourceFileName,std::ios::binary);
	OutFile = new std::ofstream(DestFileName,std::ios::binary);

	if (! *InFile || ! *OutFile)
		return false;

	ChInBuf = 0;
	IsEnd = false;
 
	while (true) 
	{
		if (!InFile->get(ch))
			break;
		IsIgnore = false;
	
		if ((ch >= 'A') && (ch <= 'Z'))
			ch = ch - 'A';
		else if ((ch >= 'a') && (ch <= 'z'))
			ch = ch - 'a' + 26;		
		else if ((ch >= '0') && (ch <= '9'))
			ch = ch - '0' + 52;	
		else if (ch == '+')
			ch = 62;		
		else if (ch == '=')
			IsEnd = true;		
		else if (ch == '/')
			ch = 63;	
		else
			IsIgnore = true; 
	
		if (!IsIgnore) 
		{	
			OutChNum = 3;
			 
			if (IsEnd) 
			{			
				if (ChInBuf == 0)
					break;				
				if ((ChInBuf == 1) || (ChInBuf == 2))
					OutChNum = 1;
				else
					OutChNum = 2;			
				ChInBuf = 3;			
			}		
			InBuf[ChInBuf++] = ch;
			if (ChInBuf == 4)
			{			
				ChInBuf = 0;

				OutBuf[0] = (InBuf[0] << 2) | ((InBuf[1] & 0x30) >> 4);			
				OutBuf[1] = ((InBuf[1] & 0x0F) << 4) | ((InBuf[2] & 0x3C) >> 2);			
				OutBuf[2] = ((InBuf[2] & 0x03) << 6) | (InBuf[3] & 0x3F);
					
				for (int i=0; i<OutChNum; i++) 
					OutFile->put(OutBuf[i]);
			}
			if (IsEnd)
				break;
		}
	} 
	InFile->close();
	OutFile->close();
	return true;
}
