// Base64.h: interface for the CBase64 class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_BASE64_H__0443E22B_E2E1_4FEA_A863_E013639D1C68__INCLUDED_)
#define AFX_BASE64_H__0443E22B_E2E1_4FEA_A863_E013639D1C68__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <stdint.h>
#include <fstream>
#include <string.h>

class CBase64  
{
protected:	
	char m_EncodingTable[64];
	void InitEncodingTable();

public:
	CBase64();
	virtual ~CBase64();
	
	//将要编码的文件编码成目的文件
	bool Encode(char* SourceFileName, char* DestFileName, short LineLen);
	//将输入字符串编码成结果字符串
	int32_t Encode(uint8_t* SourceStr, 
					 uint32_t SourceStrLen,
					 uint8_t* DestStr,
					 uint32_t DestStrLen,
					 short LineLen);
	
	//将要解码的文件解码成目的文件
	bool Decode(char* SourceFileName, char* DestFileName);
	//将输入字符串解码成结果字符串
	static bool Decode(uint8_t* SourceStr, 
				uint32_t SourceStrLen,		
				uint8_t* DestStr, 
				uint32_t* DestStrLen);
};



#endif // !defined(AFX_BASE64_H__0443E22B_E2E1_4FEA_A863_E013639D1C68__INCLUDED_)
