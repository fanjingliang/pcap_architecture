/***********************************************************************
 *     
 *  Filename    :  aesecbdec.h
 *
 *  Author      :  thm
 *  Create      :  2016-5-13
 *
 *  Description :
 *                            
 *  Copyright (C), 2016, Beijing Run.Co., Ltd.
 *
 *  ChangeLog   :             
 *
 ***********************************************************************/

#ifndef _AESECBDEC_H
#define _AESECBDEC_H
#include <openssl/aes.h>

bool AesEcbDecrypt( const unsigned char *pSrc, unsigned int nSrcLen, const char *pKey,  unsigned char *pDst);

#endif

