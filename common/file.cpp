/*********************************************************************
* 版权所有 (C)2008,锐安科技
* 
* 文件名称： file.c
* 文件标识： 
* 其它说明： 文件处理基础函数集
* 当前版本： V1.0
* 作    者： 杜欣
* 完成日期： 
*
* 修改记录1：
*    修改日期：2008年02月21日
*    版 本 号：V1.0
*    修 改 人：杜欣
*    修改内容：创建
**********************************************************************/ 
 
#include "tools/file.h"

#define SLASH '/'
/**********************************************************************
* 函数名称： file_is_exist
* 功能描述： 判断文件是否存在
* 访问的表： 无
* 修改的表： 无
* 输入参数： 1. char *szfilename : 文件的完整名称
* 输出参数： 无
* 返 回 值： 成功找到返回 1，失败返回0
* 其它说明： 无
* 修改日期        版本号     修改人       修改内容
* -----------------------------------------------
* 2008/02/21        V1.0      杜欣          创建
* 
***********************************************************************/

pthread_mutex_t file_is_exist_mutex = PTHREAD_MUTEX_INITIALIZER;

int file_is_exist( const char *szfilename )
{
        int result = 1 ;
        //同步加锁
        pthread_mutex_lock( &file_is_exist_mutex ) ;
        
        if ( access( szfilename, F_OK ) == -1)
        {
                result = 0 ;
                goto file_is_exist_err ;
        }

file_is_exist_err : 
        //同步解锁
        pthread_mutex_unlock(&file_is_exist_mutex);        
        return result ;
}

/**********************************************************************
* 函数名称： get_file_length
* 功能描述： 获取文件长度
* 访问的表： 无
* 修改的表： 无
* 输入参数： 1. FILE *f : 打开文件流的指针
* 输出参数： 无
* 返 回 值： 成功返回 文件长度，失败返回-1
* 其它说明： 无
* 修改日期        版本号     修改人       修改内容
* -----------------------------------------------
* 2008/02/21        V1.0      杜欣          创建
* 
***********************************************************************/

pthread_mutex_t get_file_length_mutex = PTHREAD_MUTEX_INITIALIZER;

//7. 获取文件长度
ssize_t get_file_length( FILE *f )
{
        int err_num = 0 ;
        struct stat lstat;
        
        //同步加锁
        pthread_mutex_lock(&get_file_length_mutex);
        
        if ( fileno( f ) == -1 )
        {
                err_num = 1 ;
                goto get_file_length_err ;
        }        
        if ( fflush( f ) != 0 )
        {
                err_num = 2 ;
                goto get_file_length_err ;
        }
        //获取文件的信息
        if ( fstat( fileno( f ), &lstat ) == -1 )
        {
                err_num = 3 ;
                goto get_file_length_err ;
        }

get_file_length_err : 
        //同步解锁
        pthread_mutex_unlock(&get_file_length_mutex);
        
        switch ( err_num )
        {
        case 3 : 
        case 2 : 
        case 1 : 
                break ; 
        }

        return ( err_num ? -1 : (ssize_t)(lstat.st_size) ) ;
}

/**********************************************************************
* 函数名称： read_dir
* 功能描述： 读取目录中的所有文件
* 访问的表： 无
* 修改的表： 无
* 输入参数： 1. const char *path                : 目录的完整路径
*            2. dir_process_func_t process_func : 处理单个文件的回调函数
*            3. void *param                     : 回调函数调用的参数
* 输出参数： 无
* 返 回 值： 成功返回 0，失败返回 非0
* 其它说明： 无
* 修改日期        版本号     修改人       修改内容
* -----------------------------------------------
* 2008/02/21        V1.0      杜欣          创建
* 
***********************************************************************/

int read_dir ( const char *path, dir_process_func_t process_func, void *param ) 
{
        int err_num = 0 ;
        struct stat dirstat;
        struct dirent *dirp = NULL ;
        DIR *dp ;

        //获取此文件/目录的属性
        if( lstat( path, &dirstat ) < 0 )
        {
                err_num = 1 ;
                goto read_dir_err ;
        }
        //判断此文件是否是目录
        if( S_ISDIR( dirstat.st_mode ) == 0 )
        {
                //否则返回错误
                err_num = 2 ;
                goto read_dir_err ;
        }

        //打开此目录
        if( ( dp = opendir( path ) ) == NULL )
        {
                //打开目录失败
                err_num = 3 ;
                goto read_dir_err ;
        }

        //循环遍历此目录下的所有文件
        while( ( dirp = readdir( dp ) ) != NULL )
        {
                process_func ( path, dirp->d_name, param ) ;
        }
        
        closedir ( dp ) ;

read_dir_err : 

        switch ( err_num ) 
        {
        case 3 : 
        case 2 : 
        case 1 : 
                break ;
        }

        return err_num ;
}

void create_dir(const char *path)
{
	char *p1,*p2;
	if( (p1 =(char *) strrchr(path,SLASH)) )	
	{
		p2 = (char *)strchr(path,SLASH);
		if (p1 == p2)
				return;
		*p1 = '\0';
		DIR *pDir = opendir(path);
		if (!pDir)
		{
			create_dir(path);
			mkdir(path, 0777);
			chmod(path, 00777);
		}
		else
		{
			closedir(pDir);
		}
		*p1 = SLASH;
	}
}
/**********************************************************************
* 函数名称： get_module_file_name
* 功能描述： 获取程序运行的当前目录
* 访问的表： 无
* 修改的表： 无
* 输入参数： 1. char* pmodule_name : 程序名称( 没有使用 )
*            2. int size           : 存放程序完整路径的缓冲区大小
* 输出参数： 1. char* pfile_name   : 程序的完整路径
* 返 回 值： 成功返回 0，失败返回 非0
* 其它说明： 无
* 修改日期        版本号     修改人       修改内容
* -----------------------------------------------
* 2008/02/21        V1.0      杜欣          创建
* 
***********************************************************************/

int get_module_file_name ( const char* pmodule_name, char* pfile_name, int size )
{
        int ret = -1 ;
        char szline [ 1024 ] = { 0 } ;
        void* lpsymbol = (void*)"" ;
        FILE *fp ;
        char *lppath = NULL ;

        unsigned long start, end;
        char *tmp;

	(void)size;
	(void)pmodule_name;

        fp = fopen ( "/proc/self/maps", "r" ) ;
        if ( fp != NULL )
        {
                while ( !feof ( fp ) )
                {
                        if ( !fgets ( szline, 
                                      sizeof ( szline ) , 
                                      fp ) )
                        {
                                continue;
                        }
                        if (    !strstr ( szline, " r-xp " ) 
                             || !strchr ( szline, '/' ) )
                        {
                                continue ;
                        }

                        sscanf ( szline, "%lx-%lx ", &start, &end ) ;
                        if (    ( lpsymbol >= ( void * ) start )
                             && ( lpsymbol <  ( void * ) end ) )
                        {
                                /* Extract the filename; it is always an absolute path */
                                lppath = strchr ( szline, '/' ) ;

                                /* Get rid of the newline */
                                tmp = strrchr ( lppath, '\n' ) ;
                                if ( tmp ) *tmp = 0 ;

                                ret = 0;
                                strcpy( pfile_name, lppath );
                        }
                }
                fclose (fp);
        }
        return ret;
}

int get_module_file_path ( const char* pmodule_name, char* pfile_path, int size )
{
        char *lppos = NULL ;
        char *lpexe_name = NULL ;

        char szfile_name [ 1024 ] = {0} ;

	(void)size;

        get_module_file_name ( pmodule_name, szfile_name, 1024 ) ;

        lpexe_name = szfile_name ;
        lppos = strrchr ( lpexe_name, '/' ) ;
        if ( lppos != NULL ) 
        {
                strncpy ( pfile_path, lpexe_name, lppos - lpexe_name ) ;
                if ( strncmp ( pfile_path, "./", 2 ) == 0 ) 
                {
                        getcwd( pfile_path, 255 ) ;
                }
        }
        else
        {
                strncpy ( pfile_path, "./", 255 ) ;
        }

        return 0;
}
