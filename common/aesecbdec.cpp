/***********************************************************************
 *     
 *  Filename    :  aesecbdec.cpp
 *
 *  Author      :  gpengyuan
 *  Create      :  2016-5-13
 *
 *  Description :
 *                            
 *  Copyright (C), 2016, Beijing Run.Co., Ltd.
 *
 *  ChangeLog   :             
 *
 ***********************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "encode/aesecbdec.h"
  
/*==========================================================================================================
*函数名称：AesEcbDecrypt
*函数参数：const unsigned char 	*pSrc		[IN] 	AES加密数据
*函数参数：unsigned int 		nSrcLen		[IN] 	AES加密数据长度
*函数参数：const char 			*pKey		[IN] 	秘钥数值
*函数参数：unsigned char 		*pDst		[OUT]	解密结果 
*函数返回：bool处理结果
*函数功能：对AES算法ECB模式加密数据进行解密
*创建日期：2016-05-13
*创建人员：gpengyuan
============================================================================================================*/
bool AesEcbDecrypt( const unsigned char *pSrc, unsigned int nSrcLen, const char *pKey,  unsigned char *pDst)
{
	if(pSrc == NULL || nSrcLen == 0 || pKey == NULL || pDst == NULL)
	{
		return false;
	}	
	if (nSrcLen % 8 != 0)
	{
		return false;
	} 
	AES_KEY AesKey;
	if (AES_set_decrypt_key((const unsigned char *)pKey, 128,  &AesKey) != 0)
	{
		return false;
	}
	unsigned int i = 0;
    for(i = 0; i < nSrcLen; i+=16)
    AES_ecb_encrypt(pSrc+i, pDst+i, &AesKey, AES_DECRYPT);
	
	return true; 	
}



