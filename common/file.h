#ifndef _FILE_H_
#define _FILE_H_

#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <dirent.h>
 
//目录处理回调函数定义
typedef int ( *dir_process_func_t ) ( const char *path, 
                                      const char *name, 
                                      void *param ) ;

int file_is_exist( const char *szfilename ) ;
ssize_t get_file_length( FILE *f ) ;
int read_dir ( const char *path, dir_process_func_t process_func, void *param ) ;
void create_dir(const char *path);
int get_module_file_name ( const char* pmodule_name, char* pfile_name, int size ) ;
int get_module_file_path ( const char* pmodule_name, char* pfile_path, int size ) ;

#endif
